import Environment from "../environment";
import Api from "./ApiService";

export const SugerenciaService = {
  validarDetalleNotificacion: async (id, idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/validarDetalleNotificacion/${id}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },
  notificacion: async (idnotificacion) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/notificacion/${idnotificacion}`,
      "GET"
    );
    return dataResult;
  },
  notificacionesAnalista: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/notificacionesAnalista/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },
};
