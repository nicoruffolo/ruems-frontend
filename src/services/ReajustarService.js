import Environment from "../environment";
import Api from "./ApiService";

export const ReajustarService = {
  verificarOracionExistente: async (idoracion, idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/verificarOracionExistente/${idoracion}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },
  verificarReajusteRequerimiento: async (idoracion) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/verificarReajusteRequerimiento/${idoracion}`,
      "GET"
    );
    return dataResult;
  },
  reajustarRequerimiento: async (idoracion) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/reajustarRequerimiento/${idoracion}`,
      "GET"
    );
    return dataResult;
  },

  aplicarReajuste: async (data) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/reajustar/`,
      "POST",
      data
    );
    return dataResult;
  },
};
