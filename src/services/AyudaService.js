import Environment from "../environment";
import Api from "./ApiService";

export const AyudaService = {
  ListarAyudas: async () => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/ayudas/ayudasListado`,
      "GET"
    );
    return dataResult;
  },
};

