import Environment from "../environment";
import Api from "./ApiService";

export const ValidarService = {
  ListadoRequerimientos: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/requerimientoAvotar/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  VotarRequerimiento: async (data) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/votarRequerimientos`,
      "POST",
      data
    );
    return dataResult;
  },

  ultimaVotacion: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/ultimaVotacion/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },
};
