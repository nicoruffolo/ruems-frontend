import Environment from "../environment";
import Api from "./ApiService";


export const EstadisticasService = {
    ListarEstadisticas: async (proyecto) => {
        const dataResult = await Api.fetch(
          `${Environment.api}api/oraciones/estadisticas/${proyecto}`,
          "GET"
        );
        return dataResult;
      },


      ValidarRequerimiento: async (idOracion) => {
        const dataResult = await Api.fetch(
          `${Environment.api}api/oraciones/validarRequerimiento/${idOracion}`,
          "PUT"
        );
        return dataResult;
      },

};