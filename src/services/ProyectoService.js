import Environment from "../environment";
import Api from "./ApiService";

export const ProyectoService = {
  proyectosDeUsuario: async () => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/proyectosUsuario`,
      "GET"
    );
    return dataResult;
  },
  obtenerIDProyecto: async (nombre) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/recuperarIdProyecto/${nombre}`,
      "GET"
    );
    return dataResult;
  },
};
