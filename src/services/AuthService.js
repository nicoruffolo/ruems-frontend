// import Environment from "environment";
export const AuthService = {
  currentUserValue: () => {
    return JSON.parse(localStorage.getItem("currentUser"));
  },

  logout: () => {
    localStorage.removeItem("currentUser");
    localStorage.removeItem("access");
  },

  getToken() {
    return `Bearer ${JSON.parse(localStorage.getItem("access")).token}`;
  },
};
