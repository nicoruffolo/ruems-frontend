import Environment from "../environment";
import Api from "./ApiService";

export const RequerimientoAnalizadoService = {
  listar: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/requerimientosAnalizados/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  aceptarRequerimiento: async (idoracion) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/aceptarRequerimiento/${idoracion}`,
      "PUT"
    );
    return dataResult;
  },

  rechazarRequerimiento: async (idoracion) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/rechazarRequerimiento/${idoracion}`,
      "PUT"
    );
    return dataResult;
  },

};
