export const ValidatorService = {
  permisoUsuario: async () => {
    if (localStorage.getItem("access") == null) {
      return false;
    } else {
      if (localStorage.getItem("proyecto") == null) {
        return false;
      } else {
        if (
          JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA"
        ) {
          return false;
        }
      }
    }
    return true;
  },

  permisoAnalista: async () => {
    if (localStorage.getItem("access") == null) {
      return false;
    } else {
      if (localStorage.getItem("proyecto") == null) {
        return false;
      } else {
        if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
          return false;
        }
      }
    }
    return true;
  },
};
