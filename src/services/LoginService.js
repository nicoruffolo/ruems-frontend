import Environment from "../environment";
import i18n from "i18n";
export const LoginService = {
  login: async (user) => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept-Language": i18n.language,
      },
      body: JSON.stringify(user),
    };

    return fetch(`${Environment.api}api/login_check`, requestOptions).then(
      (response) => {
        if (response.ok) {
          return response.json().then((data) => {
            // localStorage.setItem("access", data.access);
            return data;
          });
        }
        throw response;
      }
    );
  },
};
