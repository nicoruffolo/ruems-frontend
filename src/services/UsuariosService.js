import Environment from "../environment";
import Api from "./ApiService";

export const UsuarioService = {
  usuarioActual: async () => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/usuarioActual`,
      "GET"
    );
    return dataResult;
  },

  ListarUsuarios: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/usuarioListado/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  buscarUsuarios: async () => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/buscarUsuarios/`,
      "GET"
    );
    return dataResult;
  },

  porcentajeUsuarios: async (idusuario,idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/porcentajeHistorialUsuario/${idusuario}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  historialUsuarios: async (idusuario,idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/historialUsuario/${idusuario}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  validacionHistorial: async (idusuario,idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/validacionhistorialUsuario/${idusuario}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },

  agregarUsuarioAProyecto: async (idusuario,idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/usuarios/agregarUsuarioAProyecto/${idusuario}/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },


  register: async (data) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/register`,
      "POST",
      data
    );
    return dataResult;
  },
};


