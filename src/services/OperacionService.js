import Environment from "../environment";
import Api from "./ApiService";

export const OperacionService = {
  requerimientosRechazados: async (idProyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/operaciones/requerimientosRechazados/${idProyecto}`,
      "GET"
    );
    return dataResult;
  },
  requerimientosValidados: async (idProyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/operaciones/requerimientosValidados/${idProyecto}`,
      "GET"
    );
    return dataResult;
  },
};