import Environment from "../environment";
import Api from "./ApiService";

export const NotificacionesService = {
  listadoNotificaciones: async (idproyecto) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/sugerenciasListado/${idproyecto}`,
      "GET"
    );
    return dataResult;
  },
  confirmarNotificacion: async (idsugerencia, respuesta) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/sugerencias/confirmarSugerencia/${idsugerencia}/${respuesta}`,
      "PUT"
    );
    return dataResult;
  },
};
