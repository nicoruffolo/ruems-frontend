import Environment from "../environment";
import Api from "./ApiService";

export const RequerimientosService = {
  eliminarRequerimientos: async (data) => {
    const dataResult = await Api.fetch(
      `${Environment.api}api/oraciones/eliminarRequerimientos`,
      "POST",
      data
    );
    return dataResult;
  },

 
};