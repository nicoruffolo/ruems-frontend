import React from "react";
import Chip from "@material-ui/core/Chip";
import { useStyles } from "./styles";

const BarChip = ({ color = "primary", label }) => {
  const classes = useStyles();

  function onClick() {
    localStorage.removeItem("proyecto");
    window.location.href = "/proyectos";
  }

  return (
    <Chip
      classes={{
        root: classes.root,
        colorPrimary: classes.colorPrimary,
        label: classes.label,
      }}
      color={color}
      label={label}
      onClick={onClick}
    />
  );
};

export default BarChip;
