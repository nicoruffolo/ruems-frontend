import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 4,
    padding: 4,
    height: theme.spacing(3),
  },
  colorPrimary: {
    background: theme.palette.background.default,
    color: theme.palette.text.primary,
  },
  label: {
    fontWeight: 500,
    fontSize: 12,
  },
}));
export default useStyles;
