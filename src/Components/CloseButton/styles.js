import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  iconClose: {
    cursor: "pointer",
  },
}));
export default useStyles;
