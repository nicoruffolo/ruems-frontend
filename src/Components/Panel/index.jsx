import React, { Component } from "react";
import ProyectoBar from "Components/ProyectoBar";

const jumbotronStyle = {
  paddingBottom: "80px",
  boxShadow: "100px 100px 100px 100px rgba(0, 33,0,0)",
  backgroundColor: "#00aae4",
};

class Home extends Component {
  render() {
    return (
      <div>
        <div className="card-panel grey lighten-2" style={jumbotronStyle}>
          <div className="container">
            <br></br>
            {localStorage.getItem("idioma") === "Español" ? (
              <h1>Panel De Usuario</h1>
            ) : (
              <h1>Welcome to the User Panel</h1>
            )}
          </div>
          {localStorage.getItem("idioma") === "Español" ? (
            <strong>
              {`Usted se ha identificado como: "${
                JSON.parse(localStorage.getItem("currentUser")).username
              }"`}
            </strong>
          ) : (
            <strong>
              {`Your username is: "${
                JSON.parse(localStorage.getItem("currentUser")).username
              }"`}
            </strong>
          )}
        </div>
        <ProyectoBar />
      </div>
    );
  }
}

export default Home;
