import React, { useState, useEffect, useRef } from "react";
//import "./styles.css";

const STATUS = {
  STARTED: "Started",
  STOPPED: "Stopped",
};

const style = {
  backgroundColor: "orange",
};

export default function CountdownApp({ valor }) {
  const INITIAL_COUNT = valor;
  const [secondsRemaining, setSecondsRemaining] = useState(INITIAL_COUNT);
  const [status, setStatus] = useState(STATUS.STOPPED);
  const secondsToDisplay = secondsRemaining % 60;
  const minutesRemaining = (secondsRemaining - secondsToDisplay) / 60;
  const minutesToDisplay = minutesRemaining % 60;
  const hoursToDisplay = (minutesRemaining - minutesToDisplay) / 60;

  useEffect(() => {
    setStatus(STATUS.STARTED);
  }, []);

  // const handleStart = () => {
  //   setStatus(STATUS.STARTED);
  //  };
  //  const handleStop = () => {
  //    setStatus(STATUS.STOPPED);
  //  };
  //  const handleReset = () => {
  //   setStatus(STATUS.STOPPED);
  //   setSecondsRemaining(INITIAL_COUNT);
  // };
  useInterval(
    () => {
      if (secondsRemaining > 0) {
        setSecondsRemaining(secondsRemaining - 1);
      } else {
        window.location.reload();
      }
    },
    status === STATUS.STARTED ? 1000 : null
  );
  return (
    <div className="App">
      <div className="jumbotron" style={style}>
        <h4>Ya has votado requerimientos el dia de hoy</h4>
        <br></br>
        <h4>Espere:</h4>
        <div style={{ padding: 20 }}>
          <h1>
            <strong>
              {twoDigits(hoursToDisplay)}:{twoDigits(minutesToDisplay)}:
              {twoDigits(secondsToDisplay)}
            </strong>
          </h1>
        </div>
        <br></br>
        <h4>para volver a votar</h4>
      </div>
    </div>
  );
}

function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}
const twoDigits = (num) => String(num).padStart(2, "0");
