import React from "react";

import clsx from "clsx";

import Dialog from "@material-ui/core/Dialog";
import { Typography } from "@material-ui/core";

import AlertFooter from "Components/AlertFooter";
import AlertIcon from "Components/AlertIcon";
import CloseButton from "Components/CloseButton";
import { useStyles } from "./styles";

const AlertDialog = ({
  open,
  handleClose,
  title,
  description,
  actionAccept,
  alertType,
  showCheckbox = false,
  showCancel = true,
}) => {
  const classes = useStyles();
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      BackdropProps={{ classes: { root: classes.backdropBg } }}
    >
      <div className={classes.root}>
        <div className={classes.closeIcon}>
          <CloseButton handleClose={handleClose} fontSize={"inherit"} />
        </div>
        <div className={classes.body}>
          <AlertIcon action={alertType} />
          <div className={classes.container}>
            <Typography variant="h2" color="inherit">
              {title}
            </Typography>
            <Typography variant="h5" color={"textSecondary"}>
              {description}
            </Typography>
          </div>
        </div>

        <div
          className={clsx(classes.footer, {
            [classes.moveRight]: !showCheckbox,
          })}
        >
          <AlertFooter
            alertType={alertType}
            handleAccept={actionAccept}
            handleClose={handleClose}
            showCheckbox={showCheckbox}
            showCancel={showCancel}
          />
        </div>
      </div>
    </Dialog>
  );
};
export default AlertDialog;
