import React from "react";

import {
  CheckCircleOutline as SuccessIcon,
  ErrorOutline as ErrorIcon,
  ReportProblemOutlined as WarningIcon,
} from "@material-ui/icons";

import { useStyles } from "./styles";

const AlertIcon = ({ action }) => {
  const classes = useStyles();

  return action === "error" ? (
    <ErrorIcon
      className={`${classes.errorIcon} ${classes.icon}`}
      fontSize={"inherit"}
    />
  ) : action === "warning" ? (
    <WarningIcon
      className={`${classes.warningIcon} ${classes.icon}`}
      fontSize={"inherit"}
    />
  ) : action === "success" ? (
    <SuccessIcon
      className={`${classes.successIcon} ${classes.icon}`}
      fontSize={"inherit"}
    />
  ) : (
    false
  );
};

export default AlertIcon;
