import React from "react";
import {
  Dialog,
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  makeStyles,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: 50,
  },
  actionsContainer: {
    padding: theme.spacing(2, 3),
  },
}));

const AlreadyLanguaje = ({ onConfirm, message, open }) => {
  const classes = useStyles();

  function handleOk() {
    onConfirm();
  }

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      open={open}
    >
      <DialogContent className={classes.container}>
        <DialogContentText>{message}</DialogContentText>
      </DialogContent>
      <DialogActions className={classes.actionsContainer}>
        {localStorage.getItem("idioma") === "Español" ? (
          <Button onClick={handleOk} color="primary">
            Aceptar
          </Button>
        ) : (
          <Button onClick={handleOk} color="primary">
            Accept
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};

export default AlreadyLanguaje;
