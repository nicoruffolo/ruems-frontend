import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  barContainer: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    height: theme.spacing(7),
    width: "100%",
    boxShadow: "1px 0px 4px rgba(0, 0, 0, 0.25)",
    clipPath: "inset(-5px 0px -5px 0px)",
    marginTop: theme.spacing(2),
    background: theme.palette.secondary.white,
  },
  titulo: {
    color: theme.palette.error.light,
    fontWeight: 600,
  },
  item: {
    marginRight: theme.spacing(3),
  },
}));
export default useStyles;
