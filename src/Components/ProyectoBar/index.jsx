import React from "react";
import Typography from "@material-ui/core/Typography";

import { useStyles } from "./styles";
import BarChip from "Components/BarChip";

const ProyectoBar = () => {
  const classes = useStyles();

  const proyectoMandatario = JSON.parse(localStorage.getItem("proyecto"));

  const titulo = proyectoMandatario.nombre;

  return (
    <div
      class="app-card alert alert-dismissible shadow-sm mb-4 border-left-decoration"
      role="alert" style={{ backgroundColor: "#58BDA3" }}
    >
      <div class="inner">
        <div class="app-card-body p-3 p-lg-4">
          <h3 class="mb-3">  {`Proyecto: ${titulo}`}  </h3>
          <div class="row gx-5 gy-3">
            <div class="col-14 col-lg-12">
              <BarChip label="Salir de este Proyecto" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProyectoBar;
