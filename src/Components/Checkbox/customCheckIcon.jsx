import React from "react";
import { SvgIcon } from "@material-ui/core";

export const CustomCheckboxIcon = (props) => {
  return (
    <SvgIcon {...props}>
      <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M 18.976 4.996 L 18.976 18.976 L 4.996 18.976 L 4.996 4.996 L 18.976 4.996 M 18.976 3 L 4.996 3 C 3.897 3 3 3.897 3 4.996 L 3 18.976 C 3 20.075 3.897 20.972 4.996 20.972 L 18.976 20.972 C 20.075 20.972 20.972 20.075 20.972 18.976 L 20.972 4.996 C 20.972 3.897 20.075 3 18.976 3 Z"
          fill="#CECECE"
        />
        <path
          d="M 9.694 17.383 L 5.35 13.038 L 7.328 11.058 L 9.694 13.43 L 16.607 6.51 L 18.586 8.49 L 9.694 17.383 Z"
          fill="#2962FF"
        />
      </svg>
    </SvgIcon>
  );
};

export default CustomCheckboxIcon;
