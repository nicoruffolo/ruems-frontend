import React from "react";
import { Typography } from "@material-ui/core";

import Checkbox from "Components/Checkbox";
import Button from "Components/Button";

import { useStyles } from "./styles";

const AlertFooter = ({
  alertType,
  handleClose,
  handleAccept,
  showCheckbox = false,
  showCancel = true,
}) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      {showCheckbox && (
        <div>
          <div className={classes.checkboxTypography}>
            <Checkbox name={"alert-checkbox"} />
            <Typography color={"textSecondary"}>No mostrar de nuevo</Typography>
          </div>
        </div>
      )}
      {alertType === "info" || alertType === "success" ? (
        <div className={classes.footerButton}>
          <Button
            handleClick={handleAccept}
            size="small"
            color={"success"}
            label={
              localStorage.getItem("idioma") === "Español"
                ? "Continuar"
                : "Continue"
            }
          />
        </div>
      ) : (
        <div className={`${classes.footerButton} ${classes.footerFlex}`}>
          {showCancel && (
            <div className={classes.footerButtonSpace}>
              {localStorage.getItem("idioma") === "Español" ? (
                <Button
                  handleClick={handleClose}
                  variant="outlined"
                  label="Cancelar"
                  size="small"
                />
              ) : (
                <Button
                  handleClick={handleClose}
                  variant="outlined"
                  label="Cancel"
                  size="small"
                />
              )}
            </div>
          )}
          {localStorage.getItem("idioma") === "Español" ? (
            <Button
              handleClick={handleAccept}
              size="small"
              color={alertType === "warning" ? "warning" : "success"}
              label="Continuar"
            />
          ) : (
            <Button
              handleClick={handleAccept}
              size="small"
              color={alertType === "warning" ? "warning" : "success"}
              label="Continue"
            />
          )}
        </div>
      )}
    </React.Fragment>
  );
};

export default AlertFooter;
