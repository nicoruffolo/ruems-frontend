import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
//import InputBase from '@material-ui/core/InputBase';
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
//import MenuIcon from '@material-ui/icons/Menu';
//import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from "@material-ui/icons/AccountCircle";
//import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from "@material-ui/icons/Notifications";
import MoreIcon from "@material-ui/icons/MoreVert";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
// import Link from "@material-ui/core/Link";
import DesktopWindowsRoundedIcon from "@material-ui/icons/DesktopWindowsRounded";
import { NotificacionesService } from "services/NotificacionesService";
import { ValidatorService } from "services/ValidatorService";

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },

  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}));

export default function PrimarySearchAppBar(data = null) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [notificaciones, setNotificaciones] = React.useState([]);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const history = useHistory();

  useEffect(() => {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        NotificacionesService.listadoNotificaciones(proyectoid)
          .then((data) => {
            setNotificaciones(data.data);
          })
          .catch((error) => {
            console.log(error);
          });
      }
    });
  }, [data]);

  const home = () => {
    history.push("/inicio");
  };

  const registrar = () => {
    history.push("/Registro");
  };

  const validar = () => {
    history.push("/validaciones");
  };

  const regla = () => {
    history.push("/reglas");
  };

  const notificacion = () => {
    history.push("/notificaciones");
  };

  const handleProfileMenuOpen = (event) => {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === false) {
        window.location.reload();
      }
    });
    setAnchorEl(event.currentTarget);

  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === false) {
        window.location.href = window.location.pathname;
      }
    });
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const cerrarSesion = () => {
    localStorage.removeItem("access");
    localStorage.removeItem("currentUser");
    localStorage.removeItem("proyecto");
    history.push("/");
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {localStorage.getItem("idioma") === "Español" ? (
        <MenuItem onClick={cerrarSesion}>Cerrar Sesion</MenuItem>
      ) : (
        <MenuItem onClick={cerrarSesion}>Log Out</MenuItem>
      )}
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      {localStorage.getItem("idioma") === "Español" ? (
        <MenuItem>
          <Button type="submit" onClick={registrar}>
            Registrar Requerimiento
          </Button>
        </MenuItem>
      ) : (
        <MenuItem>
          <Button type="submit" onClick={registrar}>
            Register requirement
          </Button>
        </MenuItem>
      )}
      {localStorage.getItem("idioma") === "Español" ? (
        <MenuItem>
          <Button type="submit" onClick={validar}>
            Validar Requerimientos
          </Button>
        </MenuItem>
      ) : (
        <MenuItem>
          <Button type="submit" onClick={validar}>
            Validate Requirements
          </Button>
        </MenuItem>
      )}
      {localStorage.getItem("idioma") === "Español" ? (
        <MenuItem>
          <Button type="submit" onClick={regla}>
            Ver Reglas
          </Button>
        </MenuItem>
      ) : (
        <MenuItem>
          <Button type="submit" onClick={regla}>
            View Rules
          </Button>
        </MenuItem>
      )}
      {localStorage.getItem("idioma") === "Español" ? (
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge badgeContent={notificaciones.length} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <Button type="submit" onClick={notificacion}>
            Notificaciones
          </Button>
        </MenuItem>
      ) : (
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge badgeContent={notificaciones.length} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
          <Button type="submit" onClick={notificacion}>
            Notifications
          </Button>
        </MenuItem>
      )}
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>
          {localStorage.getItem("access") !== null &&
            localStorage.getItem("currentUser") !== null &&
            localStorage.getItem("proyecto") !== null &&
            `${JSON.parse(localStorage.getItem("currentUser")).username}`}
        </p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <DesktopWindowsRoundedIcon fontSize="large" onClick={home} />

          <Typography
            onClick={home}
            className={classes.title}
            variant="h6"
            noWrap
          >
            Ruems Requerimients
          </Typography>

          <Tabs aria-label="simple tabs example">
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              {localStorage.getItem("idioma") === "Español" ? (
                <Tab onClick={registrar} label="Registrar Requerimiento" />
              ) : (
                <Tab onClick={registrar} label="Register Requirement" />
              )}
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              {localStorage.getItem("idioma") === "Español" ? (
                <Tab onClick={validar} label="Validar Requerimientos" />
              ) : (
                <Tab onClick={validar} label="Validate Requirements" />
              )}
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              {localStorage.getItem("idioma") === "Español" ? (
                <Tab onClick={regla} label="Ver Reglas" />
              ) : (
                <Tab onClick={regla} label="View Rules" />
              )}
            </div>
          </Tabs>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton
              onClick={notificacion}
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={notificaciones.length} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
}
