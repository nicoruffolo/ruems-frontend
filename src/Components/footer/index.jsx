import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import ConfirmationDialog from "Components/ConfirmationDialog";
import AlreadyLanguaje from "Components/AlreadyLanguaje";
import es from "./img/spain.png";
import en from "./img/united-kingdom.png";
import "./footer.css";
function Footer() {
  const history = useHistory();
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openConfirm2, setOpenConfirm2] = useState(false);
  const [ready, setReady] = useState(false);
  const [ready2, setReady2] = useState(false);
  function handleSpanish() {
    if (localStorage.getItem("idioma") === "Español") {
      setReady(true);
    } else {
      setOpenConfirm(true);
    }
  }
  function handleEnglish() {
    if (localStorage.getItem("idioma") === "Ingles") {
      setReady2(true);
    } else {
      setOpenConfirm2(true);
    }
  }
  function aceptarSpanish() {
    setOpenConfirm(false);
    localStorage.setItem("idioma", "Español");
    const url = window.location.pathname;
    history.push(url);
  }
  function aceptarEnglish() {
    setOpenConfirm2(false);
    localStorage.setItem("idioma", "Ingles");
    const url = window.location.pathname;
    history.push(url);
  }
  return (
    <div>
      <ConfirmationDialog
        open={openConfirm}
        onConfirm={aceptarSpanish}
        onClose={() => setOpenConfirm(false)}
        message={`Do you want to change the language of the application to Spanish?`}
      />
      <ConfirmationDialog
        open={openConfirm2}
        onConfirm={aceptarEnglish}
        onClose={() => setOpenConfirm2(false)}
        message={`¿Desea cambiar el idioma de la aplicacion a Ingles?`}
      />
      <AlreadyLanguaje
        open={ready}
        onConfirm={() => setReady(false)}
        message={`La aplicacion ya esta configurada en idioma Español`}
      />
      <AlreadyLanguaje
        open={ready2}
        onConfirm={() => setReady2(false)}
        message={`The application is already configured in English language`}
      />
      <footer class="text-center text-white fixed-bottom" id="clase2">
        <div class="text-center p-3" id="clase1">
          © 2021 Copyright:
          <a class="text-white">"Ruem Requirements"</a>
          <div className="banderas">
            <button type="button" onClick={() => handleSpanish()}>
              <img width={25} src={es} alt="" />
            </button>
            <button type="button" onClick={() => handleEnglish()}>
              <img width={25} src={en} alt="" />
            </button>
          </div>
        </div>
      </footer>
    </div>
  );
}
export default Footer;
