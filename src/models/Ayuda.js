class Ayuda {
    id = null;
    texto = null;
    numero_ayuda = null;

    constructor(obj) {
        // IF AN OBJECT WAS PASSED THEN INITIALISE PROPERTIES FROM THAT OBJECT
        for (var prop in obj) this[prop] = obj[prop];
    }
}

export default Ayuda;