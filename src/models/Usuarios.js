class Usuarios {
    user__id = null;
    user__username = null;
    user__is_active = null;

    constructor(obj) {
        // IF AN OBJECT WAS PASSED THEN INITIALISE PROPERTIES FROM THAT OBJECT
        for (var prop in obj) this[prop] = obj[prop];
    }
}

export default Usuarios;