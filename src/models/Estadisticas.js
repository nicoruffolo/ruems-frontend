class Estadisticas {
    id = null;
    oracion = null;
    porcentaje_aprobacion = null;
    porcentaje_rechazo = null;
    porcentaje_sin_respuesta = null;

    constructor(obj) {
        // IF AN OBJECT WAS PASSED THEN INITIALISE PROPERTIES FROM THAT OBJECT
        for (var prop in obj) this[prop] = obj[prop];
    }
}

export default Estadisticas;