class RequerimientoAnalizado {
    id = null;
    texto = null;
    analisis = null;
    reglas_cumplidas = null;
    reglas_incumplidas = null;

    constructor(obj) {
        // IF AN OBJECT WAS PASSED THEN INITIALISE PROPERTIES FROM THAT OBJECT
        for (var prop in obj) this[prop] = obj[prop];
    }
}

export default RequerimientoAnalizado;