import { Switch, Route } from "react-router-dom";
//import logo from './logo.svg';
import "./App.css";
//import Navegation from './Components/Navegation';
//import Panel from './Components/Panel';
import Home from "./Pages/User/Home";
import Reglas from "./Pages/User/Reglas";
import Validacion from "./Pages/User/Validacion";
import Registro from "./Pages/User/Registro";
import Login from "./Pages/Login";
import NotFound from "./Pages/NotFound";
import Notificacion from "./Pages/User/Notifaciones";
import AdminInicio from "./Pages/Admin/AdminInicio";
import VerreglaAdmin from "./Pages/Admin/VerreglaAdmin";
import VerEstadisticaAdmin from "./Pages/Admin/VerestadisticaAdmin";
import UsersAdmin from "./Pages/Admin/AdminUsers";
import RequerimientoAnalizado from "./Pages/Admin/RequerimientoAnalizado";
import AdminaltaUser from "./Pages/Admin/AdminaltaUser";
import AdminEstadistica from "./Pages/Admin/AdminVerHistorial";
import AdminReajustar from "./Pages/Admin/AdminReajustar";
import AdminNotificacion from "Pages/Admin/AdminNotificacion";
import RequerimientoValidado from "Pages/Admin/RequerimientoValidado";
import RequerimientoRechazado from "Pages/Admin/RequerimientoRechazado";
import Proyectos from "./Pages/Proyectos";
import Footer from "Components/footer";
import { UsuarioService } from "./services/UsuariosService";


UsuarioService.usuarioActual()
        .then((data) => {
          console.log(data);
        })
        .catch((error) => {
            localStorage.clear();
            localStorage.setItem("idioma", "Español");
            if(window.location.pathname !== "/"){
              window.location.href="/";
            }     
        });

if (localStorage.getItem("idioma") === null) {
   localStorage.setItem("idioma", "Español");
  }

function App() {
  return (
    <div className="App">
      <Switch>
        {/* RUTAS DE USUARIO */}
        <Route exact path="/inicio" component={Home}></Route>
        <Route exact path="/reglas" component={Reglas}></Route>
        <Route exact path="/validaciones" component={Validacion}></Route>
        <Route exact path="/registro" component={Registro}></Route>
        <Route exact path="/notificaciones" component={Notificacion}></Route>

        {/* RUTAS DE ANALISTA */}
        <Route exact path="/home" component={AdminInicio}></Route>
        <Route exact path="/admin_reglas" component={VerreglaAdmin}></Route>
        <Route
          exact
          path="/admin_estadisticas"
          component={VerEstadisticaAdmin}
        ></Route>
        <Route exact path="/admin_usuarios" component={UsersAdmin}></Route>
        <Route
          exact
          path="/admin_requerimientos_analizados"
          component={RequerimientoAnalizado}
        ></Route>
        <Route exact path="/admin_alta_user" component={AdminaltaUser}></Route>
        <Route
          exact
          path="/admin_historial_user/:id"
          component={AdminEstadistica}
        ></Route>
        <Route
          exact
          path="/admin_reajustar/:id"
          component={AdminReajustar}
        ></Route>
        <Route
          exact
          path="/admin_notificacion/:id"
          component={AdminNotificacion}
        ></Route>

        <Route
          exact
          path="/admin_requerimientos_validados"
          component={RequerimientoValidado}
        ></Route>

        <Route
          exact
          path="/admin_requerimientos_rechazados"
          component={RequerimientoRechazado}
        ></Route>

        {/* RUTAS GENERICAS */}
        <Route exact path="/" component={Login} />
        <Route exact path="/proyectos" component={Proyectos} />
        <Route component={NotFound} />
      </Switch>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <Footer />
    </div>
  );
}

export default App;
