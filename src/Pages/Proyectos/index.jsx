import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";
import "./styles.css";
import Button from "Components/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Snackbar, SnackbarContent } from "@material-ui/core";
import { ProyectoService } from "services/ProyectoService";
import LoadingRequest from "Components/LoadingRequest";
import 'bootstrap/dist/css/bootstrap.min.css';

const Proyectos = () => {
  const [value, setValue] = React.useState("DIS");
  const [loadingSubmit, setLoadingSubmit] = React.useState(false);
  const [error, setError] = React.useState(null);
  const [loading, setLoading] = React.useState(false);
  const [loadingProject, setLoadingProject] = React.useState(false);
  const [proyectos, setProyectos] = React.useState([]);
  const json = {};

  useEffect(() => {
    setLoading(true);
    setLoadingSubmit(true);
    ProyectoService.proyectosDeUsuario()
      .then((response) => {
        setProyectos(response.data);
        setLoading(false);
        setLoadingSubmit(false);
      })
      .catch((error) => {
        if (localStorage.getItem("idioma") === "Español") {
          setError("No se ha podido conectar con el servidor");
        } else {
          setError("Could not connect to the server");
        }
        setLoading(false);
        setLoadingSubmit(false);
      });
  }, []);

  function handleChange(event) {
    setValue(event.target.value);
  }

  function onSubmit() {
    if (value === "DIS") {
      if (localStorage.getItem("idioma") === "Español") {
        setError(
          "Por Favor seleccione un proyecto de la lista antes de continuar"
        );
      } else {
        setError("Please select a project from the list before continuing");
      }
    } else {
      setLoadingProject(true);
      setLoadingSubmit(true);
      ProyectoService.obtenerIDProyecto(value)
        .then((response) => {
          json.id = parseInt(response.data[0].id);
          json.nombre = value;
          localStorage.setItem("proyecto", JSON.stringify(json));
          const usuario = JSON.parse(localStorage.getItem("currentUser"));
          if (usuario.rol === "ANALISTA") {
            window.location.href = "/home";
          } else {
            window.location.href = "/inicio";
          }
        })
        .catch((error) => {
          setLoadingSubmit(false);
          setLoadingProject(false);
          if (localStorage.getItem("idioma") === "Español") {
            setError(
              "Ha ocurrido un error al conectar con el servidor. Intentelo de nuevo"
            );
          } else {
            setError("An error occurred connecting to the server. Try again");
          }
        });
    }
  }
  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (
      localStorage.getItem("proyecto") !== null &&
      JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO"
    ) {
      return <Redirect to="/inicio" />;
    } else {
      if (
        localStorage.getItem("proyecto") !== null &&
        JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA"
      ) {
        return <Redirect to="/home" />;
      }
    }
  }
  return (
    <div class="jumbotron bg-dark text-white" >
      <div class="container">
      {localStorage.getItem("idioma") === "Español" ? (
        <h2>Seleccione un proyecto antes de continuar la Sesion:</h2>
      ) : (
        <h2>Select a project before continuing the Session:</h2>
      )}
      <br></br>
      <br></br>
      <LoadingRequest open={loadingProject} />
        <form>
          {loading && <CircularProgress />}
          {!loading && proyectos && (
            <select
            class="selectpicker" 
              aria-label=".form-select-lg example"
              required
              onChange={handleChange}
            >
              {localStorage.getItem("idioma") === "Español" ? (
                <option value="DIS" selected disabled>
                   Seleccione un proyecto
                </option>
              ) : (
                <option value="DIS" selected disabled>
                  Select a project
                </option>
              )}
              {proyectos.map((proyecto) => {
                return (
                  <option value={proyecto.nombre}>{proyecto.nombre}</option>
                );
              })}
            </select>
          )}
          <br></br>
          <br></br>
          {localStorage.getItem("idioma") === "Español" ? (
            <Button
              label="Aceptar"
              variant={"contained"}
              color={"success"}
              handleClick={onSubmit}
              disabled={loadingSubmit}
            />
          ) : (
            <Button
              label="Accept"
              variant={"contained"}
              color={"success"}
              handleClick={onSubmit}
              disabled={loadingSubmit}
            />
          )}
        </form>
      {error && (
        <Snackbar open={!!error} onClose={() => setError(null)}>
          <SnackbarContent className="error" message={error} />
        </Snackbar>
      )}
    </div>
    </div>

  );
};
export default Proyectos;
