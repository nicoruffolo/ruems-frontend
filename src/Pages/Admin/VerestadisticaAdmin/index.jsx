import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { EstadisticasService } from "../../../services/EstadisticasService";
import { ValidatorService } from "services/ValidatorService";
import Layout from "../Layout";
import Typography from "@material-ui/core/Typography";
import TablaEstadisticas from "Components/TablaEstadisticas";
import AlertDialog from "Components/AlertDialog";
import ConfirmationDialog from "Components/ConfirmationDialog";
import LoadingRequest from "Components/LoadingRequest";
import ProyectoBar from "Components/ProyectoBar";
function EstadisticaAdmin() {
  const style = {
    backgroundColor: "orange",
  };
  const [estadisticas, setEstadisticas] = useState([]);
  const [requerimiento, setRequerimiento] = useState([]);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [closeAlert, setCloseAlert] = useState(false);
  const [permiso, setPermiso] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadingAction, setLoadingAction] = useState(false);

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        setLoading(true);
        EstadisticasService.ListarEstadisticas(proyectoid)
          .then((data) => {
            setEstadisticas(data.data);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
          });
      }
    });
  }, [openAlert]);

  function validar(idrequerimiento) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setRequerimiento(idrequerimiento);
        setOpenConfirm(true);
      } else {
        window.location.reload();
      }
    });
  }

  function validacionFinal() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setOpenConfirm(false);
        setLoadingAction(true);
        EstadisticasService.ValidarRequerimiento(requerimiento)
          .then((data) => {
            setOpenAlert(false);
            setOpenAlert(true);
            setCloseAlert(true);
            setLoadingAction(false);
          })
          .catch((error) => {
            setLoadingAction(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setCloseAlert(false);
      }}
      description="Felicitaciones, el requerimiento ha sido validado de forma definitiva y cerrado para recibir votaciones"
      actionAccept={() => setCloseAlert(false)}
      title=""
      alertType="success"
    />
  );

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <div className="jumbotron" style={style}>
          <Typography variant="h5" gutterBottom>
            La siguiente tabla lleva un control en % de las respuestas de los
            usuarios a los requerimientos aceptados por los analistas. Podra
            validar aquellos requerimientos que han obtenido una respuesta
            esperada.
          </Typography>
          <ConfirmationDialog
            open={openConfirm}
            onConfirm={validacionFinal}
            onClose={() => setOpenConfirm(false)}
            message="Atencion, estas por validar este requerimiento. Si lo haces ya no obtedra mas respuestas de los usuarios. Hazlo unicamente si consideras que obtuvo una respuesta acorde. ¿Deseas validarlo?"
          />
        </div>
        <LoadingRequest open={loadingAction} />
        <div class="table-responsive-sm">
          <div className="jumbotron  bg-warning text-dark">
            <TablaEstadisticas
              rows={estadisticas}
              validar={validar}
              loading={loading}
            />
          </div>
        </div>
        {openAlert && closeAlert && alert}
      </Layout>
    </div>
  );
}

export default EstadisticaAdmin;
