import React, { useState, useEffect } from "react";
import { RequerimientoAnalizadoService } from "../../../services/RequerimientoAnalizadoService";
import { ValidatorService } from "services/ValidatorService";
import { ReajustarService } from "services/ReajustarService";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import AlertDialog from "Components/AlertDialog";
import Layout from "../Layout";
import Typography from "@material-ui/core/Typography";
import TablaRequerimientosAnalizados from "Components/TablaRequeriemientosAnalizados";
import ConfirmationDialog from "Components/ConfirmationDialog";
import ProyectoBar from "Components/ProyectoBar";
import LoadingRequest from "Components/LoadingRequest";
import { Snackbar, SnackbarContent } from "@material-ui/core";

//import Link from '@material-ui/core/Link';
function RequerimientoAnalizado() {
  const style = {
    backgroundColor: "orange",
  };
  const history = useHistory();
  const [requerimientos, setRequerimientos] = useState([]);
  const [mensaje, setMensaje] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openConfirm2, setOpenConfirm2] = useState(false);
  const [openConfirm3, setOpenConfirm3] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [closeAlert, setCloseAlert] = useState(false);
  const [openAlert2, setOpenAlert2] = useState(false);
  const [closeAlert2, setCloseAlert2] = useState(false);
  const [openAceptar, setOpenAceptar] = useState(false);
  const [openRechazar, setOpenRechazar] = useState(false);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loadingAction, setLoadingAction] = useState(false);
  const [currentrequerimiento, setCurrentRequerimiento] = useState(0);

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        setLoading(true);
        RequerimientoAnalizadoService.listar(proyectoid)
          .then((data) => {
            setRequerimientos(data.data);
            setLoading(false);
          })
          .catch((error) => {
            setError(
              "Ha ocurrido un error en el servidor. Intentelo de nuevo mas tarde"
            );
            setLoading(false);
          });
      }
    });
  }, [openAlert, openAlert2]);

  const reajustar = (id, reglas_cumplidas) => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        if (reglas_cumplidas.length === 3) {
          setMensaje(
            "Este requerimiento cumple con todas nuestras reglas. No aconsejamos reajustarlo. ¿Quieres reajustarlo de todas maneras?"
          );
        }
        if (reglas_cumplidas.length === 2) {
          setMensaje(
            "Este requerimiento cumple con dos reglas. Igualmente puede reajustarlo para cumplir con la regla que falta. ¿Quieres reajustar este requerimiento?"
          );
        }
        if (reglas_cumplidas.length === 1) {
          setMensaje(
            "Este requerimiento cumple con solo 1 regla. Recomendamos que lo reajuste ahora. ¿Quieres reajustar este requerimiento?"
          );
        }
        if (reglas_cumplidas.length === 0) {
          setMensaje(
            "Este requerimiento no cumple con ninguna de nuestras reglas. NO ACONSEJAMOS bajo ningun punto su aceptacion sin reajustarlo antes. ¿Quieres reajustar este requerimiento?"
          );
        }
        setCurrentRequerimiento(id);
        setOpenConfirm2(true);
      } else {
        window.location.reload();
      }
    });
  };

  function aceptar(id, reglas_cumplidas) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        if (reglas_cumplidas.length === 3) {
          setMensaje(
            "Este requerimiento cumple con 3/3 reglas. Recomendamos su aceptacion. ¿Quieres aceptarlo y pasarlo a produccion?"
          );
        }
        if (reglas_cumplidas.length === 2) {
          setMensaje(
            "Este requerimiento tiene una regla incumplida pero podria aceptarse sin problemas. ¿Quieres aceptarlo y pasarlo a produccion?"
          );
        }
        if (reglas_cumplidas.length === 1) {
          setMensaje(
            "Este requerimiento tiene dos reglas incumplidas. Le recomendamos que lo reajuste primero. ¿Quieres aceptarlo de todas maneras?"
          );
        }
        if (reglas_cumplidas.length === 0) {
          setMensaje(
            "Este requerimiento no cumple con ninguna de nuestras reglas. NO ACONSEJAMOS bajo ningun punto su aceptacion sin reajustarlo antes. ¿Quieres aceptarlo de todas maneras?"
          );
        }
        setCurrentRequerimiento(id);
        setOpenConfirm(true);
      } else {
        window.location.reload();
      }
    });
  }

  function rechazar(id, reglas_cumplidas) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        if (reglas_cumplidas.length === 3) {
          setMensaje(
            "Este requerimiento cumple con 3/3 reglas. No recomendamos su rechazo. ¿Quieres rechazarlo de todas maneras?"
          );
        }
        if (reglas_cumplidas.length === 2) {
          setMensaje(
            "Este requerimiento tiene 2 reglas cumplidas. No recomedamos su rechazo. ¿Quieres rechazarlo de todas maneras?"
          );
        }
        if (reglas_cumplidas.length === 1) {
          setMensaje(
            "Este requerimiento tiene solo 1 regla cumplida. Puede rechazarlo ahora mismo o reajustarlo si lo considera necesario. ¿Quieres rechazar este requerimiento?"
          );
        }
        if (reglas_cumplidas.length === 0) {
          setMensaje(
            "Este requerimiento no cumple con ninguna de nuestras reglas. Recomendamos que lo rechace ahora o lo reajuste completamente si lo considera necesario. ¿Quieres rechazar este requerimiento?"
          );
        }
        setCurrentRequerimiento(id);
        setOpenConfirm3(true);
      } else {
        window.location.reload();
      }
    });
  }

  function aceptarRequerimiento() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        RequerimientoAnalizadoService.aceptarRequerimiento(currentrequerimiento)
          .then((data) => {
            setOpenAlert(false);
            setOpenAlert(true);
            setCloseAlert(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
          })
          .catch((error) => {
            setError(
              "Ha ocurrido un error en el servidor. Intentelo de nuevo mas tarde"
            );
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  function reajustarRequerimiento() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setOpenConfirm2(false);
        history.replace(`/admin_reajustar/${currentrequerimiento}`);
      } else {
        window.location.reload();
      }
    });
  }

  function rechazarRequerimiento() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        RequerimientoAnalizadoService.rechazarRequerimiento(
          currentrequerimiento
        )
          .then((data) => {
            setOpenAlert2(false);
            setOpenAlert2(true);
            setCloseAlert2(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
          })
          .catch((error) => {
            setError(
              "Ha ocurrido un error en el servidor. Intentelo de nuevo mas tarde"
            );
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  function verificarAceptacion() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoadingSubmit(true);
        setLoadingAction(true);
        setOpenConfirm(false);
        ReajustarService.verificarReajusteRequerimiento(currentrequerimiento)
          .then((data) => {
            aceptarRequerimiento();
          })
          .catch((error) => {
            setOpenAceptar(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  function verificarRechazo() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoadingSubmit(true);
        setLoadingAction(true);
        setOpenConfirm3(false);
        ReajustarService.verificarReajusteRequerimiento(currentrequerimiento)
          .then((data) => {
            rechazarRequerimiento();
          })
          .catch((error) => {
            setOpenRechazar(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setCloseAlert(false);
      }}
      description="El requerimiento fue aceptado y puesto en produccion. A partir de ahora los usuarios podran votarlo."
      actionAccept={() => setCloseAlert(false)}
      title=""
      alertType="success"
    />
  );

  const alert2 = (
    <AlertDialog
      open={openAlert2}
      handleClose={() => {
        setCloseAlert2(false);
      }}
      description="El requerimiento fue rechazado completamente. Ya no podra ser aceptado ni reajustado"
      actionAccept={() => setCloseAlert2(false)}
      title=""
      alertType="success"
    />
  );

  const alert3 = (
    <AlertDialog
      open={openAceptar}
      handleClose={() => {
        setOpenAceptar(false);
      }}
      description="No puede aceptar este requerimiento porque tiene un reajuste pendiente de confirmacion"
      actionAccept={() => setOpenAceptar(false)}
      title=""
      alertType="error"
    />
  );

  const alert4 = (
    <AlertDialog
      open={openRechazar}
      handleClose={() => {
        setOpenRechazar(false);
      }}
      description="No puede rechazar este requerimiento porque tiene un reajuste pendiente de confirmacion"
      actionAccept={() => setOpenRechazar(false)}
      title=""
      alertType="error"
    />
  );

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <div className="jumbotron" style={style}>
          <Typography variant="h5" gutterBottom>
            A continuacion se muestran los requerimientos pendientes de
            confirmacion.
          </Typography>
        </div>
        <LoadingRequest open={loadingAction} />
        <ConfirmationDialog
          open={openConfirm}
          onConfirm={verificarAceptacion}
          onClose={() => setOpenConfirm(false)}
          message={mensaje}
        />
        <ConfirmationDialog
          open={openConfirm2}
          onConfirm={reajustarRequerimiento}
          onClose={() => setOpenConfirm2(false)}
          message={mensaje}
        />
        <ConfirmationDialog
          open={openConfirm3}
          onConfirm={verificarRechazo}
          onClose={() => setOpenConfirm3(false)}
          message={mensaje}
        />
        <div class="table-responsive-sm">
          <div className="jumbotron bg-warning text-white">
            <TablaRequerimientosAnalizados
              rows={requerimientos}
              aceptar={aceptar}
              rechazar={rechazar}
              reajustar={reajustar}
              loading={loading}
              loadingSubmit={loadingSubmit}
            />
          </div>
        </div>
        {openAlert && closeAlert && alert}
        {openAlert2 && closeAlert2 && alert2}
        {openAceptar && alert3}
        {openRechazar && alert4}
        {error && (
          <Snackbar open={!!error} onClose={() => setError(null)}>
            <SnackbarContent className="error" message={error} />
          </Snackbar>
        )}
      </Layout>
    </div>
  );
}

export default RequerimientoAnalizado;
