import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";
import { ValidatorService } from "services/ValidatorService";
import Layout from "../Layout";
import Typography from "@material-ui/core/Typography";
import AlertDialog from "Components/AlertDialog";
import { ReajustarService } from "services/ReajustarService";
import TextField from "@material-ui/core/TextField";
import LoadingRequest from "Components/LoadingRequest";
import ConfirmationDialog from "Components/ConfirmationDialog";
import ProyectoBar from "Components/ProyectoBar";
import Button from "Components/Button";

const AdminReajustar = ({ match }) => {
  const history = useHistory();
  const [error, setError] = useState(null);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [requerimiento, setRequerimiento] = useState([]);
  const [currentReajuste, setCurrentReajuste] = useState({
    texto: "",
    oracion: match.params.id,
  });
  const style = {
    backgroundColor: "orange",
  };
  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  function reajustarRequerimiento(idoracion) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        ReajustarService.reajustarRequerimiento(idoracion)
          .then((data) => {
            setRequerimiento(data.data);
            setLoading(false);
          })
          .catch((error) => {
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  function verificarReajuste(idoracion) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        ReajustarService.verificarReajusteRequerimiento(idoracion)
          .then((data) => {
            reajustarRequerimiento(idoracion);
          })
          .catch((error) => {
            setError(
              "No puede reajustar este requerimiento porque ya tiene un reajuste anterior pendiente de confirmacion."
            );
            setLoading(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoading(true);
        const ide = match.params.id;
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        ReajustarService.verificarOracionExistente(ide, proyectoid)
          .then((data) => {
            verificarReajuste(ide);
          })
          .catch((error) => {
            setError(
              "El ID del requerimiento recibido no es valido para reajustar"
            );
            setLoading(false);
          });
      }
    });
  }, [match]);

  const validate = (values) => {
    const errors = {};
    if (!values.texto) {
      errors.texto = "Este campo es requerido";
    } else if (values.texto.trim() === "") {
      errors.texto = "Por favor ingrese algun cambio valido";
    } else if (values.texto.includes("\n")) {
      errors.texto =
        "Por favor no presione saltos de linea al rejustar un requerimiento";
    } else if (/(\s{2,})/g.test(values.texto)) {
      errors.texto = "Por favor deje solo 1 espacio entre cada palabra";
    }
    return errors;
  };

  useEffect(() => {
    formik.setValues(currentReajuste);
  }, [currentReajuste]);

  const handleSubmit = async (reajuste) => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        console.log(reajuste);
        setCurrentReajuste(reajuste);
        setOpenConfirm(true);
      } else {
        window.location.reload();
      }
    });
  };

  function aplicarReajusteRequerimiento() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setOpenConfirm(false);
        setLoadingSubmit(true);
        setLoading(true);
        ReajustarService.aplicarReajuste(currentReajuste)
          .then((data) => {
            setOpenAlert(true);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
            setLoadingSubmit(false);
            setError("Ocurrio un error en el servidor");
          });
      } else {
        window.location.reload();
      }
    });
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        history.push(`/admin_requerimientos_analizados`);
      }}
      description="Has reajustado exitosamente el requerimiento. Le informaremos al autor del mismo sobre tu reajuste. El autor podria aceptarlo o rechazarlo"
      actionAccept={() => history.push(`/admin_requerimientos_analizados`)}
      title=""
      alertType="success"
    />
  );

  const formik = useFormik({
    initialValues: currentReajuste,
    onSubmit: (values) => handleSubmit(values),
    validate,
  });

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar />
        <br></br>
        <br></br>
        <LoadingRequest open={loading} />
        {error && (
          <div class="alert alert-danger" role="alert">
            <strong>{error}</strong>
          </div>
        )}
        <ConfirmationDialog
          open={openConfirm}
          onConfirm={aplicarReajusteRequerimiento}
          onClose={() => setOpenConfirm(false)}
          message={`¿Estas seguro que quieres aplicar los cambios reajustados al requerimiento N° ${formik.values.oracion}?`}
        />
        {!loading && requerimiento && !error && (
          <div>
            <div className="jumbotron" style={style}>
              <Typography variant="h4" gutterBottom>
                Reajuste el siguiente requerimiento tanto como lo considere
                necesario. Luego presione "Enviar" para notificarle al usuario
                creador los cambios corregidos. El usuario podria aceptar su
                sugerencia o solicitar un nuevo reajuste.
              </Typography>
            </div>
            <div className="jumbotron bg-warning text-dark">
              <h1>
                <strong>Requerimiento a reajustar:</strong>
              </h1>
              <br />
              <textarea
                class="form-control rounded-0"
                id="exampleFormControlTextarea1"
                rows="10"
                disabled
              >
                {requerimiento.texto}
              </textarea>
            </div>

            <br />

            <div>
              <h1>
                <strong>Ingrese el requerimiento con sus ajustes</strong>
              </h1>
              <br />
              <div className="jumbotron bg-warning text-dark">
                <form onSubmit={formik.handleSubmit}>
                  <TextField
                    id="standard-multiline-flexible"
                    style={{ backgroundColor: "white" }}
                    placeholder="Escriba algo aqui"
                    name="texto"
                    spellcheck="true"
                    multiline
                    fullWidth
                    rows={10}
                    value={formik.values.texto}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.errors.texto && formik.touched.texto}
                    helperText={
                      formik.touched.texto ? formik.errors.texto : null
                    }
                    variant="filled"
                  />
                  <TextField
                    id="standard-multiline-flexible"
                    type="hidden"
                    name="oracion"
                    value={formik.values.oracion}
                  />
                  <br />
                  <br />
                  <Button
                    label="Enviar"
                    variant={"contained"}
                    type="submit"
                    color={"primary"}
                    disabled={loadingSubmit}
                  />
                </form>
              </div>
            </div>
          </div>
        )}
        <Button
          label="Volver"
          variant={"contained"}
          color={"success"}
          handleClick={() => history.push(`/admin_requerimientos_analizados`)}
          disabled={loadingSubmit}
        />
        {openAlert && alert}
      </Layout>
    </div>
  );
};

export default AdminReajustar;
