import React from "react";
import "./style.css";
const Inicio = () => {
  return (
    <div>
      <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Inicio</h1>
        </div>

        <div class="row">
          <div class="col-lg-6">
            <div class="card mb-4">
              <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">
                  Sobre los requerimientos
                </h6>
              </div>
              <div class="card-body">
                Los requerimientos que sean cargados por los usuarios seran
                enviados automaticamente a la tabla de "requerimientos
                analizados". En dicha tabla podras aceptarlos y rechazarlos de
                forma directa. En ocasiones algunos requerimientos necesitaran
                ser reajustados para su posterior confirmacion.
              </div>
            </div>

            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Estadisticas</h6>
              </div>
              <div class="card-body">
                En esta seccion podras consultar porcentajes y promedios sobre
                los requerimientos activos y las respuestas que los usuarios les
                proporcionaron. Tendras la posibilidad de cerrar (validar) un
                requerimiento cuando consideres que el mismo alcanzo el
                porcentaje adecuado.
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="card shadow mb-4">
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">
                  Sobre las reglas
                </h6>
              </div>

              <div class="card-body">
                El sistema ya cuenta con una serie de reglas predefinidas que
                ayudaran con el analisis y procesamiento de los requerimientos
                que sean dados de alta por los usuarios. Estas reglas no pueden
                ser eliminadas, pero podras cargar nuevas reglas.
              </div>
            </div>

            <div class="card shadow mb-4">
              <p class="d-block card-header py-3" data-toggle="collapse">
                <h6 class="m-0 font-weight-bold text-primary">Usuarios</h6>
              </p>

              <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                  Como Analista del sitio tendras la posibilidad de administrar
                  a los usuarios del sistema. Entre las opciones disponibles se
                  encuentran, el alta de usuarios nuevos (analistas o usuarios
                  naturales), consultar el historial de los usuarios comunes
                  para saber que respuestas han proporcionado a los
                  requerimientos y podras ademas desactivarlos en caso que sea
                  necesario.
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Inicio;
