import React from "react";
import { Redirect } from "react-router-dom";
import Layout from "../Layout";
import Inicio from "../Inicio";
import ProyectoBar from "Components/ProyectoBar";

//import Regla from "../VerreglaAdmin";

const AdminInicio = () => {
  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }
  return (
    <div>
      <Layout>
        <ProyectoBar />
        <Inicio />
      </Layout>
    </div>
  );
};
export default AdminInicio;
