import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { ValidatorService } from "services/ValidatorService";
import { UsuarioService } from "../../../services/UsuariosService";
import Layout from "../Layout";
import Typography from "@material-ui/core/Typography";
import TablaUsuarios from "Components/TablaUsuarios";
import ProyectoBar from "Components/ProyectoBar";
import LoadingRequest from "Components/LoadingRequest";


//import Button from '@material-ui/core/Button';
function UsersAdmin() {
  
  const history = useHistory();
  const [usuarios, setUsuarios] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadingAction, setLoadingAction] = useState(false);

  const style = {
    backgroundColor: "orange",
  };

  const agregaruser = () => {
    history.push(`/admin_alta_user`);
  };

  const historial = (id) => {
    history.replace(`/admin_historial_user/${id}`);
  };

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }


  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        setLoading(true);
        UsuarioService.ListarUsuarios(proyectoid)
          .then((data) => {
            setUsuarios(data.data);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
          });
      }
    });
  }, []);

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <div className="jumbotron" style={style}>
          <Typography variant="h4" gutterBottom>
            El siguiente listado muestra los usuarios colaboradores de este
            proyecto
          </Typography>
        </div>

        <br />

        <button type="button" onClick={agregaruser} class="btn btn-primary">
          {" "}
          Agregar Usuario al Proyecto{" "}
        </button>

        <br />
        <br />
        <div class="table-responsive-sm">
          <div className="jumbotron bg-warning text-white">
            <h5>
              Miembros del Proyecto "
              {JSON.parse(localStorage.getItem("proyecto")).nombre}"
            </h5>
            <br></br>
            <TablaUsuarios
              rows={usuarios}
              historial={historial}
              loading={loading}
            />
          </div>
        </div>
      </Layout>
    </div>
  );
}

export default UsersAdmin;
