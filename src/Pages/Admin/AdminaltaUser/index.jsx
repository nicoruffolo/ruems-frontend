import React, { useState, useEffect } from "react";
import "./index.css";
import Layout from "../Layout";
import { useFormik } from "formik";
import { Redirect } from "react-router-dom";
import { ValidatorService } from "services/ValidatorService";
import ProyectoBar from "Components/ProyectoBar";
import Typography from "@material-ui/core/Typography";
import "bootstrap/dist/css/bootstrap.min.css";
import TablaResultados from "Components/TablaResultados";
import ConfirmationDialog from "Components/ConfirmationDialog";
import LoadingRequest from "Components/LoadingRequest";
import AlertDialog from "Components/AlertDialog";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { UsuarioService } from "../../../services/UsuariosService";
import { Snackbar, SnackbarContent } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import Button from "Components/Button";

const currencies = [
  {
    value: 1,
    label: "Analista",
  },
  {
    value: 2,
    label: "Usuario",
  },
];

const AltaUser = () => {
  const [busqueda, setBusqueda] = useState("");
  const [usuarios, setUsuarios] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [usuarioRegistrado, setUsuarioRegistrado] = useState("");
  const [loadingAction, setLoadingAction] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [openAlert2, setOpenAlert2] = useState(false);
  const [closeAlert, setCloseAlert] = useState(false);
  const [usuarioActual, setUsuarioActual] = useState(0);
  const [usernameActual, setUsernameActual] = useState("");
  const [resultados, setResultados] = useState([]);
  const [currentUser, setCurrentUser] = useState({
    name: "",
    username: "",
    email: "",
    roles: "",
    password: "",
    passwordConfirm: "",
  });
  let json = {};

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        if (usuarios.length === 0) {
          setLoading(true);
          UsuarioService.buscarUsuarios()
            .then((data) => {
              setUsuarios(data.data);
              setLoading(false);
            })
            .catch((error) => {
              console.log(error);
              setLoading(false);
            });
        }
      }
    });
  }, [resultados, openAlert]);

  function filtrarElementos() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        var search = usuarios.filter((item) => {
          if (
            item.username.toString().includes(busqueda) ||
            item.email.toLowerCase().includes(busqueda) ||
            item.name.toLowerCase().includes(busqueda) ||
            item.roles[0].toLowerCase().includes(busqueda)
          ) {
            return item;
          }
        });
        setResultados(search);
      } else {
        window.location.reload();
      }
    });
  }

  function onChange(e) {
    setBusqueda(e.target.value);
    if (e.target.value !== "") {
      filtrarElementos();
    } else {
      setResultados([]);
    }
  }

  const validate = (values) => {
    const passwordRegex = /(?=.*[0-9])/;
    const errors = {};
    if (!values.name) {
      errors.name = "Este campo es requerido";
    } else if (values.name.trim() === "") {
      errors.name = "Ingrese un nombre valido";
    } else if (/(\s{2,})/g.test(values.name)) {
      errors.name =
        "Por favor deje solo 1 espacio entre cada palabra del nombre";
    }
    if (!values.username) {
      errors.username = "Este campo es requerido";
    } else if (values.username.trim() === "") {
      errors.username = "Ingrese un nombre de usuario valido";
    } else if (/(\s)/g.test(values.username)) {
      errors.username =
        "El Nombre de usuario debe contener solo una palabra sin espacios en blanco";
    }
    if (!values.email) {
      errors.email = "Este campo es requerido";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Direccion de correo electronico incorrecta";
    } else if (/(\s)/g.test(values.email)) {
      errors.email = "No agrege espacios en blanco al email";
    }
    if (!values.roles) {
      errors.roles = "Este campo es requerido";
    }
    if (!values.password) {
      errors.password = "Este campo es requerido";
    } else if (values.password.length < 4) {
      errors.password = "La contraseña debe tener al menos 4 caracteres";
    } else if (!passwordRegex.test(values.password)) {
      errors.password = "Contraseña invalida. Debe contener un número";
    }
    if (!values.passwordConfirm) {
      errors.passwordConfirm = "Este campo es requerido";
    } else if (values.password !== values.passwordConfirm) {
      errors.passwordConfirm =
        "La contraseña ingresada no coincide con la original";
    }
    return errors;
  };

  useEffect(() => {
    formik.setValues(currentUser);
  }, [currentUser]);

  function agregarRegister(data) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        const idusuario = data.data.id;
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        UsuarioService.agregarUsuarioAProyecto(idusuario, proyectoid)
          .then((response) => {
            setOpenAlert2(true);
            setUsuarioRegistrado(data.data.username);
            setCurrentUser({
              name: "",
              username: "",
              email: "",
              password: "",
              roles: "",
              passwordConfirm: "",
            });
            setLoadingAction(false);
          })
          .catch((error) => {
            setLoadingAction(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  const handleSubmit = async (user) => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoadingAction(true);
        json._name = user.name;
        json._username = user.username;
        json._email = user.email;
        json._password = user.password;
        if (user.roles === 1) {
          json._roles = ["ANALISTA"];
        } else {
          json._roles = ["USUARIO"];
        }
        UsuarioService.register(json)
          .then((data) => {
            agregarRegister(data);
          })
          .catch((error) => {
            if (error.status === 500) {
              setLoadingAction(false);
              setError(
                "El nombre de usuario y/o email ingresados ya se encuentran en uso"
              );
              setCurrentUser({
                name: user.name,
                username: user.username,
                email: user.email,
                password: "",
                roles: user.roles,
                passwordConfirm: "",
              });
            }
          });
      } else {
        window.location.reload();
      }
    });
  };

  const formik = useFormik({
    initialValues: currentUser,
    onSubmit: (values) => handleSubmit(values),
    validate,
  });

  function agregar(iduser, username) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setUsuarioActual(iduser);
        setUsernameActual(username);
        setOpenConfirm(true);
      } else {
        window.location.reload();
      }
    });
  }

  function aplicarAltaUsuario() {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setOpenConfirm(false);
        setLoadingAction(true);
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        UsuarioService.agregarUsuarioAProyecto(usuarioActual, proyectoid)
          .then((data) => {
            setOpenAlert(false);
            setOpenAlert(true);
            setCloseAlert(true);
            setResultados([]);
            setBusqueda("");
            setLoadingAction(false);
          })
          .catch((error) => {
            setLoadingAction(false);
            setError(
              `Atencion, el usuario "${usernameActual}" ya es miembro del proyecto "${
                JSON.parse(localStorage.getItem("proyecto")).nombre
              }"`
            );
          });
      } else {
        window.location.reload();
      }
    });
  }

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setCloseAlert(false);
      }}
      description={`Operacion Exitosa. El usuario "${usernameActual}" ha sido agregado al proyecto "${
        JSON.parse(localStorage.getItem("proyecto")).nombre
      }"`}
      actionAccept={() => setCloseAlert(false)}
      title=""
      alertType="success"
    />
  );

  const alert2 = (
    <AlertDialog
      open={openAlert2}
      handleClose={() => {
        setOpenAlert2(false);
      }}
      description={`Operacion Exitosa. El usuario "${usuarioRegistrado}" ha sido registrado en el sistema y ya es miembro del proyecto "${
        JSON.parse(localStorage.getItem("proyecto")).nombre
      }"`}
      actionAccept={() => setOpenAlert2(false)}
      title=""
      alertType="success"
    />
  );

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <Typography variant="h5" gutterBottom>
          Agregue un nuevo usuario al Proyecto. Busquelo o registrelo.
        </Typography>
        <br></br>
        <LoadingRequest open={loadingAction} />
        <ConfirmationDialog
          open={openConfirm}
          onConfirm={aplicarAltaUsuario}
          onClose={() => setOpenConfirm(false)}
          message={`¿Quieres agregar al usuario "${usernameActual}" al proyecto "${
            JSON.parse(localStorage.getItem("proyecto")).nombre
          }"?`}
        />
        <div className="jumbotron bg-warning text-dark">
          <div className="table-responsive">
            <div className="barraBusqueda">
              <input
                type="text"
                placeholder="Buscar Usuario"
                className="textField"
                name="busqueda"
                value={busqueda}
                onChange={onChange}
                disabled={loading}
              />
              <button type="button" className="btnBuscar" /*onClick={onClear}*/>
                {" "}
                <FontAwesomeIcon icon={faSearch} />
              </button>
            </div>
            {resultados.length > 0 && (
              <TablaResultados
                rows={resultados}
                agregar={agregar}
                busqueda={busqueda}
              />
            )}
            {resultados.length < 1 && busqueda !== "" && (
              <h4>
                No se encontraron resultados que coincidan con la busqueda
              </h4>
            )}
          </div>
        </div>
        <br></br>
        <br></br>
        <h3>
          <strong>Nuevo Usuario</strong>
        </h3>
        <br />
        <div class="table-responsive-sm">
          <div className="jumbotron bg-white text-black">
            <form onSubmit={formik.handleSubmit}>
              <div class="form-group">
                <label for="nombre">
                  <strong>Nombre</strong>
                </label>
                <TextField
                  id="outlined-full-width"
                  style={{ margin: 8 }}
                  type="text"
                  name="name"
                  placeholder="Ingrese el nombre del usuario"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.errors.name && formik.touched.name}
                  helperText={formik.touched.name ? formik.errors.name : null}
                  variant="filled"
                />
              </div>

              <div class="form-group">
                <label for="nombre">
                  <strong>Nombre de Usuario</strong>
                </label>
                <TextField
                  id="outlined-full-width"
                  style={{ margin: 8 }}
                  type="text"
                  name="username"
                  placeholder="Ingrese el Username"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.username}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.errors.username && formik.touched.username}
                  helperText={
                    formik.touched.username ? formik.errors.username : null
                  }
                  variant="filled"
                />
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">
                  <strong>Email</strong>
                </label>
                <TextField
                  id="outlined-full-width"
                  style={{ margin: 8 }}
                  type="email"
                  name="email"
                  placeholder="Ingrese el email del usuario"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.errors.email && formik.touched.email}
                  helperText={formik.touched.email ? formik.errors.email : null}
                  variant="filled"
                />
              </div>

              <div class="form-group">
                <label for="exampleFormControlSelect1">
                  <strong>Rol</strong>
                </label>
                <TextField
                  id="filled-select-currency"
                  style={{ margin: 8 }}
                  name="roles"
                  select
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.roles}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.errors.roles && formik.touched.roles}
                  helperText={formik.touched.roles ? formik.errors.roles : null}
                  variant="filled"
                >
                  {currencies.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">
                  <strong>Contraseña</strong>
                </label>
                <TextField
                  id="outlined-full-width"
                  type="password"
                  name="password"
                  style={{ margin: 8 }}
                  placeholder="Ingrese una contraseña para el usuario"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.errors.password && formik.touched.password}
                  helperText={
                    formik.touched.password ? formik.errors.password : null
                  }
                  variant="filled"
                />
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">
                  <strong>Confirmar Contraseña</strong>
                </label>
                <TextField
                  id="outlined-full-width"
                  type="password"
                  name="passwordConfirm"
                  style={{ margin: 8 }}
                  placeholder="Confirme la contraseña anterior"
                  fullWidth
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={formik.values.passwordConfirm}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={
                    formik.errors.passwordConfirm &&
                    formik.touched.passwordConfirm
                  }
                  helperText={
                    formik.touched.passwordConfirm
                      ? formik.errors.passwordConfirm
                      : null
                  }
                  variant="filled"
                />
              </div>
              <br></br>
              <Button
                label="Registrar Usuario"
                variant={"contained"}
                type="submit"
                color={"success"}
              />
            </form>
          </div>
        </div>
        {openAlert && closeAlert && alert}
        {openAlert2 && alert2}
        {error && (
          <Snackbar open={!!error} onClose={() => setError(null)}>
            <SnackbarContent className="error" message={error} />
          </Snackbar>
        )}
      </Layout>
    </div>
  );
};

export default AltaUser;
