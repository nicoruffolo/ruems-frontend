import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { AyudaService } from "../../../services/AyudaService";
import { ValidatorService } from "services/ValidatorService";
import Layout from "../Layout";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import ProyectoBar from "Components/ProyectoBar";
import CircularProgress from "@material-ui/core/CircularProgress";

const style = {
  backgroundColor: "orange",
};

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const ListarAyudasView = () => {
  const classes = useStyles();

  const [ayudas, setAyudas] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoading(true);
        AyudaService.ListarAyudas()
          .then((data) => {
            setAyudas(data);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
          });
      }
    });
  }, []);

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <div className="jumbotron" style={style}>
          <Typography variant="h4" gutterBottom>
            Reglas Predefinidas del Sistema
          </Typography>
        </div>
        <br></br>
        <div className="jumbotron  bg-warning text-dark">
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Regla</StyledTableCell>
                  <StyledTableCell align="center">#Regla</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {loading && (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <CircularProgress />
                    </TableCell>
                  </TableRow>
                )}
                {!loading &&
                  ayudas &&
                  ayudas.map((ayuda) => {
                    return (
                      <StyledTableRow>
                        <StyledTableCell component="th" scope="row">
                          {ayuda.texto}
                        </StyledTableCell>
                        <StyledTableCell align="center">
                          {ayuda.numero_ayuda}
                        </StyledTableCell>
                      </StyledTableRow>
                    );
                  })}
                {ayudas.length === 0 && !loading && (
                  <p className="messages">No existen ayudas para mostrar</p>
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </Layout>
    </div>
  );
};
export default ListarAyudasView;
