import React, { useState, useEffect } from "react";
import Layout from "../Layout";
import "./index.css";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Button from "Components/Button";
import ProyectoBar from "Components/ProyectoBar";
import TablaHistorial from "Components/TablaHistorial";
import LoadingRequest from "Components/LoadingRequest";
import { UsuarioService } from "services/UsuariosService";
import { ValidatorService } from "services/ValidatorService";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const Porcentajehistorial = ({ match }) => {
  const [loading, setLoading] = useState(false);
  const [carga, setCarga] = useState(null);
  const [error, setError] = useState(null);
  const [historial, setHistorial] = useState([]);
  const [usuario, setUsuario] = useState([]);
  const [porcentaje, setPorcentaje] = useState([]);
  const [loadingAction, setLoadingAction] = useState(false);

  const history = useHistory();

  function historialUsuarios(idoracion, idproyecto) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
    UsuarioService.historialUsuarios(idoracion, idproyecto)
      .then((data) => {
        setHistorial(data.data);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
    } else {
      window.location.reload();
    }
  });
  }

  function porcentajeUsuarios(idoracion, idproyecto) {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
    UsuarioService.porcentajeUsuarios(idoracion, idproyecto)
      .then((data) => {
        setPorcentaje(data.data);
        setCarga(true);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
      });
       } else {
      window.location.reload();
    }
  });
  }


  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }


  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoading(true);
        const ide = match.params.id;
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        UsuarioService.validacionHistorial(ide, proyectoid)
          .then((data) => {
            setUsuario(data.data);
            historialUsuarios(ide, proyectoid);
            porcentajeUsuarios(ide, proyectoid);
          })
          .catch((error) => {
            setError("No puede ver el historial del usuario indicado");
            setLoading(false);
          });
      }
    });
  }, [match]);

  function Example(props) {
    return (
      <div style={{ marginBottom: 80 }}>
        <div style={{ marginTop: 30 }}>
          <div style={{ width: "120%" }}>
            <h3 className="h5">
              <strong>{props.label}</strong>
            </h3>
            <p>{props.description}</p>
          </div>
          <div style={{ width: "80%", paddingRight: 30 }}>{props.children}</div>
        </div>
      </div>
    );
  }

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }
  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <LoadingRequest open={loading} />
        {error && (
          <div>
            <div class="alert alert-danger" role="alert">
              <strong>{error}</strong>
            </div>
            <br></br>
            <br></br>
            <Button
              label="Volver"
              variant={"contained"}
              color={"success"}
              size="small"
              handleClick={() => history.push(`/admin_usuarios`)}
            />
          </div>
        )}
        {!loading && historial && porcentaje && carga && !error && (
          <div>
            <Typography variant="h4" gutterBottom>
              La siguiente tabla muestra el historial del usuario{" "}
              <strong>"{usuario[0].username}"</strong>
            </Typography>
            <br></br>
            <div className="jumbotron  bg-warning text-dark">
              <div className="container">
                <div class="skills">
                  <Example label="Correctas">
                    <CircularProgressbar
                      value={Math.trunc(porcentaje.correctas)}
                      text={`${Math.trunc(porcentaje.correctas)}%`}
                      styles={buildStyles({
                        pathColor: "green",
                      })}
                    />
                  </Example>
                  <Example label="Incorrectas">
                    <CircularProgressbar
                      value={Math.trunc(porcentaje.incorrectas)}
                      text={`${Math.trunc(porcentaje.incorrectas)}%`}
                      styles={buildStyles({
                        pathColor: "red",
                      })}
                    />
                  </Example>
                  <Example label="Sin Respuesta">
                    <CircularProgressbar
                      value={Math.trunc(porcentaje.nose)}
                      text={`${Math.trunc(porcentaje.nose)}%`}
                    />
                  </Example>
                </div>
              </div>
            </div>
            {/* <br /> */}
            <div className="jumbotron bg-warning text-dark">
              <TablaHistorial rows={historial} loading={loading} />
              <br></br>
              <br></br>
              <Button
                label="Volver"
                variant={"contained"}
                color={"success"}
                size="small"
                handleClick={() => history.push(`/admin_usuarios`)}
              />
            </div>
          </div>
        )}
      </Layout>
    </div>
  );
};

export default Porcentajehistorial;
