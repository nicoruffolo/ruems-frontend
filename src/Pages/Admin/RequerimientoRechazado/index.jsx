import React, { useState, useEffect } from "react";
import Layout from "../Layout";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Button from "Components/Button";
import ProyectoBar from "Components/ProyectoBar";
import TablaRequerimientos from "Components/TablaRequerimientos";
import LoadingRequest from "Components/LoadingRequest";
import { OperacionService } from "services/OperacionService";
import { ValidatorService } from "services/ValidatorService";
import "react-circular-progressbar/dist/styles.css";

const RequerimientoRechazado = () => {
   const [loading, setLoading] = useState(false);
   const [carga, setCarga] = useState(null);
   const [error, setError] = useState(null);
   const [requerimientos, setRequerimientos] = useState([]);

 const history = useHistory();


  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }


  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoading(true);
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        OperacionService.requerimientosRechazados(proyectoid)
          .then((data) => {
            setRequerimientos(data.data);
            setLoading(false);
            setCarga(true);
          })
          .catch((error) => {
            setError("No pudo conectar con el servidor");
            setLoading(false);
          });
      }
    });
  }, []);


  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }
  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
         <LoadingRequest open={loading} /> 
         {error && (
          <div>
            <div class="alert alert-danger" role="alert">
              <strong>{error}</strong>
            </div>
            
          </div>
        )} 
         {!loading  && carga && !error && requerimientos.length > 0   && ( 
          <div>
            <Typography variant="h4" gutterBottom>
              La siguiente tabla muestra aquellos requerimientos que fueron rechazados {" "}
            </Typography>
            <br></br>
           
            <div className="jumbotron bg-warning text-dark">
              <TablaRequerimientos title="Requerimientos Rechazados" rows={requerimientos} loading= { loading }/> 
              <br></br>
              <br></br>
             
              <Button
                label="Volver"
                variant={"contained"}
                color={"success"}
                size="small"
                handleClick={() => history.push(`/inicio`)}
              />
            </div>
          </div>
          )}
          {carga &&  requerimientos.length === 0 && <h4> No existen requerimientos para mostrar </h4>}
      </Layout>
    </div>
  );
};

export default RequerimientoRechazado;