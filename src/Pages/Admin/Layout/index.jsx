import React from "react";
import { SugerenciaService } from "../../../services/SugerenciaService";
function cerrarSesion() {
  localStorage.removeItem("access");
  localStorage.removeItem("currentUser");
  localStorage.removeItem("proyecto");
  window.location.href = "/";
}

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notificaciones: [],
    };

    this.fechaArmar = this.fechaArmar.bind(this);
  }

  componentDidMount() {
    const proyecto = JSON.parse(localStorage.getItem("proyecto"));
    const proyectoid = proyecto.id;
    SugerenciaService.notificacionesAnalista(proyectoid)
      .then((data) => {
        var temp = data.data;
        this.setState({ notificaciones: [...temp] });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  fechaArmar(fecha) {
    const año = parseInt(fecha[0]);
    const mes = parseInt(fecha[1]);
    const dia = parseInt(fecha[2]);
    var fechaFinal = new Date(año, mes - 1, dia);
    var options = { year: "numeric", month: "long", day: "numeric" };
    var resultado = fechaFinal.toLocaleDateString("es-ES", options);
    return resultado;
  }

  render() {
    return (
      <div id="wrapper">
        <ul
          class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
          id="accordionSidebar"
        >
          <a
            class="sidebar-brand d-flex align-items-center justify-content-center"
            href="/home"
          >
            <div class="sidebar-brand-text mx-3">Analista</div>
          </a>

          <hr class="sidebar-divider my-0" />
          {window.location.pathname !== "/home" ? (
            <li class="nav-item">
              <a class="nav-link" href="/home">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                
                <span> Inicio</span>
              </a>
            </li>
          ) : (
            <li class="nav-item active">
              <a class="nav-link" href="/home">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Inicio</span>
              </a>
            </li>
          )}

          <hr class="sidebar-divider" />

          <div class="sidebar-heading">Opciones</div>

          {window.location.pathname !== "/admin_requerimientos_analizados" ? (
            <li class="nav-item">
              <p
                class="nav-link"
                data-toggle="collapse"
                data-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                <i class="fas fa-fw fa-cog"></i>
                <span>Requerimientos Analizados</span>
              </p>
              <div
                id="collapseTwo"
                class="collapse show"
                aria-labelledby="headingTwo"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Consulte aquellos</h6>
                  <h6 class="collapse-header">requerimientos pendientes</h6>
                  <h6 class="collapse-header">de confirmacion</h6>
                  <a
                    class="collapse-item active"
                    href="/admin_requerimientos_analizados"
                  >
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            window.location.pathname ===
              "/admin_requerimientos_analizados" && (
              <li class="nav-item active">
                <p
                  class="nav-link"
                  data-toggle="collapse"
                  data-target="#collapseTwo"
                  aria-expanded="true"
                  aria-controls="collapseTwo"
                >
                  <i class="fas fa-fw fa-cog"></i>
                  <span>Requerimientos Analizados</span>
                </p>
                <div
                  id="collapseTwo"
                  class="collapse show"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionSidebar"
                >
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Consulte aquellos</h6>
                    <h6 class="collapse-header">requerimientos pendientes</h6>
                    <h6 class="collapse-header">de confirmacion</h6>
                    <a
                      class="collapse-item active"
                      href="/admin_requerimientos_analizados"
                    >
                      Ver
                    </a>
                  </div>
                </div>
              </li>
            )
          )}


{window.location.pathname !== "/admin_requerimientos_validados" ? (
            <li class="nav-item">
              <p
                class="nav-link"
                data-toggle="collapse"
                data-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                <i class="fas fa-fw fa-cog"></i>
                <span>Requerimientos Validados</span>
              </p>
              <div
                id="collapseTwo"
                class="collapse show"
                aria-labelledby="headingTwo"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Consulte aquellos</h6>
                  <h6 class="collapse-header">requerimientos aprobados</h6>
                  <a
                    class="collapse-item active"
                    href="/admin_requerimientos_validados"
                  >
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            window.location.pathname ===
              "/admin_requerimientos_validados" && (
              <li class="nav-item active">
                <p
                  class="nav-link"
                  data-toggle="collapse"
                  data-target="#collapseTwo"
                  aria-expanded="true"
                  aria-controls="collapseTwo"
                >
                  <i class="fas fa-fw fa-cog"></i>
                  <span>Requerimientos Validados</span>
                </p>
                <div
                  id="collapseTwo"
                  class="collapse show"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionSidebar"
                >
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Consulte aquellos</h6>
                    <h6 class="collapse-header">requerimientos aprobados</h6>
                    <a
                      class="collapse-item active"
                      href="/admin_requerimientos_validados"
                    >
                      Ver
                    </a>
                  </div>
                </div>
              </li>
            )
          )}

{window.location.pathname !== "/admin_requerimientos_rechazados" ? (
            <li class="nav-item">
              <p
                class="nav-link"
                data-toggle="collapse"
                data-target="#collapseTwo"
                aria-expanded="false"
                aria-controls="collapseTwo"
              >
                <i class="fas fa-fw fa-cog"></i>
                <span>Requerimientos Rechazados</span>
              </p>
              <div
                id="collapseTwo"
                class="collapse show"
                aria-labelledby="headingTwo"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Consulte aquellos</h6>
                  <h6 class="collapse-header">requerimientos rechazados</h6>
                  <a
                    class="collapse-item active"
                    href="/admin_requerimientos_rechazados"
                  >
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            window.location.pathname ===
              "/admin_requerimientos_rechazados" && (
              <li class="nav-item active">
                <p
                  class="nav-link"
                  data-toggle="collapse"
                  data-target="#collapseTwo"
                  aria-expanded="true"
                  aria-controls="collapseTwo"
                >
                  <i class="fas fa-fw fa-cog"></i>
                  <span>Requerimientos Rechazados</span>
                </p>
                <div
                  id="collapseTwo"
                  class="collapse show"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionSidebar"
                >
                  <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Consulte aquellos</h6>
                    <h6 class="collapse-header">requerimientos rechazados</h6>
                    <a
                      class="collapse-item active"
                      href="/admin_requerimientos_rechazados"
                    >
                      Ver
                    </a>
                  </div>
                </div>
              </li>
            )
          )}

          {window.location.pathname !== "/admin_reglas" ? (
            <li class="nav-item">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapseUtilities"
                aria-expanded="true"
                aria-controls="collapseUtilities"
              >
                <i class="fas fa-fw fa-wrench"></i>
                <span>Administrar Reglas</span>
              </p>
              <div
                id="collapseUtilities"
                class="collapse"
                aria-labelledby="headingUtilities"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Configuracion de</h6>
                  <h6 class="collapse-header">las reglas del</h6>
                  <h6 class="collapse-header">sistema</h6>
                  <a class="collapse-item" href="/admin_reglas">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            <li class="nav-item active">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapseUtilities"
                aria-expanded="true"
                aria-controls="collapseUtilities"
              >
                <i class="fas fa-fw fa-wrench"></i>
                <span>Administrar Reglas</span>
              </p>
              <div
                id="collapseUtilities"
                class="collapse"
                aria-labelledby="headingUtilities"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Configuracion de</h6>
                  <h6 class="collapse-header">las reglas del</h6>
                  <h6 class="collapse-header">sistema</h6>
                  <a class="collapse-item" href="/admin_reglas">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          )}

          <hr class="sidebar-divider" />

          <div class="sidebar-heading">Otras opciones</div>

          {window.location.pathname !== "/admin_estadisticas" ? (
            <li class="nav-item">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapsePages"
                aria-expanded="true"
                aria-controls="collapsePages"
              >
                <i class="fas fa-fw fa-folder"></i>
                <span>Estadisticas</span>
              </p>
              <div
                id="collapsePages"
                class="collapse"
                aria-labelledby="headingPages"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Consulte los</h6>
                  <h6 class="collapse-header">numeros y calculos</h6>
                  <h6 class="collapse-header">mas importantes en</h6>
                  <h6 class="collapse-header">el desempeño</h6>
                  <h6 class="collapse-header">del sistema</h6>
                  <a class="collapse-item" href="/admin_estadisticas">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            <li class="nav-item active">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapsePages"
                aria-expanded="true"
                aria-controls="collapsePages"
              >
                <i class="fas fa-fw fa-folder"></i>
                <span>Estadisticas</span>
              </p>
              <div
                id="collapsePages"
                class="collapse"
                aria-labelledby="headingPages"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Consulte los</h6>
                  <h6 class="collapse-header">numeros y calculos</h6>
                  <h6 class="collapse-header">mas importantes en</h6>
                  <h6 class="collapse-header">el desempeño</h6>
                  <h6 class="collapse-header">del sistema</h6>
                  <a class="collapse-item" href="/admin_estadisticas">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          )}

          <hr class="sidebar-divider d-none d-md-block" />

          {window.location.pathname !== "/admin_usuarios" ? (
            <li class="nav-item">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapseUsers"
                aria-expanded="true"
                aria-controls="collapseUsers"
              >
                <i class="fas fa-fw fa-folder"></i>
                <span>Usuarios</span>
              </p>
              <div
                id="collapseUsers"
                class="collapse"
                aria-labelledby="headingUsers"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Modulo de</h6>
                  <h6 class="collapse-header">administracion y</h6>
                  <h6 class="collapse-header">configuracion de</h6>
                  <h6 class="collapse-header">usuarios</h6>
                  <h6 class="collapse-header">del sistema</h6>
                  <a class="collapse-item" href="/admin_usuarios">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          ) : (
            <li class="nav-item active">
              <p
                class="nav-link collapsed"
                data-toggle="collapse"
                data-target="#collapseUsers"
                aria-expanded="true"
                aria-controls="collapseUsers"
              >
                <i class="fas fa-fw fa-folder"></i>
                <span>Usuarios</span>
              </p>
              <div
                id="collapseUsers"
                class="collapse"
                aria-labelledby="headingUsers"
                data-parent="#accordionSidebar"
              >
                <div class="bg-white py-2 collapse-inner rounded">
                  <h6 class="collapse-header">Modulo de</h6>
                  <h6 class="collapse-header">administracion y</h6>
                  <h6 class="collapse-header">configuracion de</h6>
                  <h6 class="collapse-header">usuarios</h6>
                  <h6 class="collapse-header">del sistema</h6>
                  <a class="collapse-item" href="/admin_usuarios">
                    Ver
                  </a>
                </div>
              </div>
            </li>
          )}
        </ul>

        <div id="content-wrapper" class="d-flex flex-column">
          <div id="content">
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
              <button
                id="sidebarToggleTop"
                class="btn btn-link d-md-none rounded-circle mr-3"
              >
                <i class="fa fa-bars"></i>
              </button>

              <h2>Panel de Analista</h2>

              <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown no-arrow mx-1">
                  <p
                    class="nav-link dropdown-toggle"
                    id="alertsDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i class="fas fa-bell fa-fw"></i>

                    <span class="badge badge-danger badge-counter">
                      {this.state.notificaciones.length}
                    </span>
                  </p>
                  <div
                    class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                    aria-labelledby="alertsDropdown"
                  >
                    {this.state.notificaciones.length === 0 ? (
                      <h6 class="dropdown-header">
                        No hay notificaciones nuevas
                      </h6>
                    ) : (
                      <h6 class="dropdown-header">Nuevas Notificaciones</h6>
                    )}
                    {this.state.notificaciones.map((notificacion) =>
                      notificacion.respuesta === false ? (
                        <button
                          class="dropdown-item d-flex align-items-center"
                          onClick={() =>
                            (window.location.href = `/admin_notificacion/${notificacion.sugerencia}`)
                          }
                        >
                          <div class="mr-3">
                            <div class="icon-circle bg-primary">
                              <i class="fas fa-file-alt text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">
                              {this.fechaArmar(
                                String(notificacion.fecha)
                                  .split("T")[0]
                                  .split("-")
                              )}
                            </div>
                            <span class="font-weight-bold">
                              {`El usuario ${notificacion.username} acepto la sugerencia N° ${notificacion.sugerencia}`}
                            </span>
                          </div>
                        </button>
                      ) : (
                        <button
                          class="dropdown-item d-flex align-items-center"
                          onClick={() =>
                            (window.location.href = `/admin_notificacion/${notificacion.sugerencia}`)
                          }
                        >
                          <div class="mr-3">
                            <div class="icon-circle bg-warning">
                              <i class="fas fa-exclamation-triangle text-white"></i>
                            </div>
                          </div>
                          <div>
                            <div class="small text-gray-500">
                              {this.fechaArmar(
                                String(notificacion.fecha)
                                  .split("T")[0]
                                  .split("-")
                              )}
                            </div>
                            <span class="font-weight-bold">
                              {`El usuario ${notificacion.username} rechazo la sugerencia N° ${notificacion.sugerencia}`}
                            </span>
                          </div>
                        </button>
                      )
                    )}
                  </div>
                </li>

                <div class="topbar-divider d-none d-sm-block"></div>

                <li class="nav-item dropdown no-arrow">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                    {`${
                      JSON.parse(localStorage.getItem("currentUser")).username
                    }`}
                  </span>
                  <img
                    alt=""
                    class="img-profile rounded-circle"
                    src="css/undraw_profile.svg"
                    width="40px"
                    height="40px"
                    id="userDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  />

                  <div
                    class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                    aria-labelledby="userDropdown"
                  >
                    <button
                      class="dropdown-item"
                      href="#"
                      data-toggle="modal"
                      data-target="#logoutModal"
                    >
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Cerrar Sesion
                    </button>
                  </div>
                </li>
              </ul>
            </nav>

            {this.props.children}

            <a class="scroll-to-top rounded" href="#page-top">
              <i class="fas fa-angle-up"></i>
            </a>
            <div
              class="modal fade"
              id="logoutModal"
              tabindex="-1"
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                      ¿Seguro que quieres salir?
                    </h5>
                    <button
                      class="close"
                      type="button"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Seleccione "Cerrar sesión" si está listo para finalizar su
                    sesión.
                  </div>
                  <div class="modal-footer">
                    <button
                      class="btn btn-secondary"
                      type="button"
                      data-dismiss="modal"
                    >
                      Cancelar
                    </button>
                    <br></br>
                    <button
                      onClick={cerrarSesion}
                      class="btn btn-primary"
                      type="button"
                    >
                      Cerrar Sesion
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Layout;
