import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { SugerenciaService } from "services/SugerenciaService";
import { ValidatorService } from "services/ValidatorService";
import Layout from "../Layout";
import ProyectoBar from "Components/ProyectoBar";
import LoadingRequest from "Components/LoadingRequest";
import Button from "Components/Button";

const AdminNotificacion = ({ match }) => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [notificacion, setNotificacion] = useState([]);
  const [error, setError] = useState(null);

  function detalleNotificacion(idsugerencia) {
    SugerenciaService.notificacion(idsugerencia)
      .then((data) => {
        setNotificacion(data.data);
        setLoading(false);
      })
      .catch((error) => {
        setError("Ocurrio un error en el servidor");
        setLoading(false);
      });
  }

  useEffect(() => {
    ValidatorService.permisoAnalista().then((response) => {
      if (response === true) {
        setLoading(true);
        const ide = match.params.id;
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        SugerenciaService.validarDetalleNotificacion(ide, proyectoid)
          .then((data) => {
            detalleNotificacion(ide);
          })
          .catch((error) => {
            setError("El ID de notificacion recibido es invalido");
            setLoading(false);
          });
      }
    });
  }, [match]);

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO") {
        return <Redirect to="/inicio" />;
      }
    }
  }

  return (
    <div className="App">
      <Layout>
        <ProyectoBar></ProyectoBar>
        <br></br>
        <br></br>
        <LoadingRequest open={loading} />
        {error && (
          <div class="alert alert-danger" role="alert">
            <strong>{error}</strong>
          </div>
        )}
        {!loading && notificacion.length > 0 && (
          <div class="jumbotron bg-dark text-white">
            {console.log(notificacion)}
            <div class="form-group">
              <label for="exampleFormControlSelect1">
                {localStorage.getItem("idioma") === "Español" ? (
                  <h4>
                    <strong>Requerimiento Original</strong>
                  </h4>
                ) : (
                  <h4>
                    <strong>Original Requirement</strong>
                  </h4>
                )}
              </label>
              <br />
              <textarea
                class="text-white"
                name="descripcion"
                rows="5"
                cols="50"
                disabled
              >
                {notificacion[0].original}
              </textarea>
            </div>
            <label for="exampleFormControlSelect1">
              {localStorage.getItem("idioma") === "Español" ? (
                <h4>
                  <strong>Reajuste Propuesto</strong>
                </h4>
              ) : (
                <h4>
                  <strong>Proposed Readjustment</strong>
                </h4>
              )}
            </label>
            <br />
            <textarea
              class="text-white"
              name="descripcion"
              rows="5"
              cols="50"
              disabled
            >
              {notificacion[0].texto}
            </textarea>
            <br></br>
            <br></br>
            {notificacion[0].respuesta === false ? (
              <div class="alert alert-success" role="alert">
                <strong>
                  El usuario "{notificacion[0].username}" acepto el reajuste
                  propuesto. El requerimiento ha pasado a produccion
                </strong>
              </div>
            ) : (
              <h4>
                <div>
                  <div class="alert alert-danger" role="alert">
                    <strong>
                      El usuario "{notificacion[0].username}" rechazo el
                      reajuste propuesto.
                    </strong>
                  </div>
                  <br></br>
                  <br></br>
                  <Button
                    label="Volver a Reajustar"
                    variant={"contained"}
                    color={"success"}
                    handleClick={() =>
                      history.replace(
                        `/admin_reajustar/${notificacion[0].oracion_id}`
                      )
                    }
                  />
                </div>
              </h4>
            )}
          </div>
        )}
      </Layout>
    </div>
  );
};
export default AdminNotificacion;
