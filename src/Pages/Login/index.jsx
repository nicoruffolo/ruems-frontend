import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";
import "./styles.css";
import foto from "./foto.png";
import { LoginService } from "../../services/LoginService";
import { UsuarioService } from "services/UsuariosService";
import { TextField, Button } from "@material-ui/core";
import LoadingRequest from "Components/LoadingRequest";
import { Snackbar, SnackbarContent } from "@material-ui/core";

const Login = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [error, setError] = useState(null);
  const [label, setLabel] = useState("");
  const [label2, setLabel2] = useState("");
  const [currentUser, setCurrentUser] = useState({
    _username: "",
    _password: "",
  });

  function registrarUsuario() {
    UsuarioService.usuarioActual()
      .then((data) => {
        localStorage.setItem("currentUser", JSON.stringify(data.data));
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const handleSubmit = async (user) => {
    setLoadingSubmit(true);
    setLoading(true);
    LoginService.login(user)
      .then((data) => {
        localStorage.setItem("access", JSON.stringify(data));
        registrarUsuario();
        history.push("/proyectos");
      })
      .catch((error) => {
        if (localStorage.getItem("idioma") === "Español") {
          setError("El usuario y/o contraseña son incorrectos");
        } else {
          setError("The username or password is incorrect");
        }
        setLoadingSubmit(false);
        setLoading(false);
        setCurrentUser({ _username: user._username, _password: "" });
      });
  };

  const validate = (values) => {
    const errors = {};
    if (!values._username) {
      if (localStorage.getItem("idioma") === "Español") {
        errors._username = "Campo requerido";
      } else {
        errors._username = "This field is required";
      }
    }
    if (!values._password) {
      if (localStorage.getItem("idioma") === "Español") {
        errors._password = "Campo requerido";
      } else {
        errors._password = "This field is required";
      }
    }
    return errors;
  };

  const formik = useFormik({
    initialValues: currentUser,
    onSubmit: (values) => handleSubmit(values),
    validate,
  });

  useEffect(() => {
    if (localStorage.getItem("idioma") === "Español") {
      setLabel("Usuario");
      setLabel2("Contraseña");
    } else {
      setLabel("User");
      setLabel2("Password");
    }
    formik.setValues(currentUser);
  }, [currentUser]);

  if (
    localStorage.getItem("access") !== null &&
    localStorage.getItem("proyecto") == null
  ) {
    return <Redirect to="/proyectos" />;
  } else {
    if (
      localStorage.getItem("proyecto") !== null &&
      localStorage.getItem("proyecto") !== null &&
      JSON.parse(localStorage.getItem("currentUser")).rol === "USUARIO"
    ) {
      return <Redirect to="/inicio" />;
    } else {
      if (
        localStorage.getItem("proyecto") !== null &&
        localStorage.getItem("proyecto") !== null &&
        JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA"
      ) {
        return <Redirect to="/home" />;
      }
    }
  }

  return (
    <div>
      <div className="centro">
        <div class="jumbotron">
          <h1>
            {localStorage.getItem("idioma") === "Español" ? (
              <strong>  RUEM Requirements </strong>
            ) : (
              <strong>  RUEM Requirements </strong>
            )}
          </h1>
          <br />
          {localStorage.getItem("idioma") === "Español" ? (
            <h3> Herramienta colaborativa de definicion de requerimientos.</h3>
          ) : (
            <h3> Collaborative requirements definition app.</h3>
          )}
        </div>
      </div>
      <LoadingRequest open={loading} />
      <div class="container-fluid">
        <div class="row no-gutter">
          <div class="col-md-6 d-none d-md-flex bg-image">
            <img src={foto} alt="" width="700" height="700" />
          </div>
          <div class="col-md-6 bg-light">
            <div class="login d-flex align-items-center py-5">
              <div class="container">
                <div class="row">
                  <div class="col-lg-10 col-xl-7 mx-auto">
                    {localStorage.getItem("idioma") === "Español" ? (
                      <h3 class="display-4">Iniciar Sesion</h3>
                    ) : (
                      <h3 class="display-4">Log In</h3>
                    )}
                    <br />
                    <form onSubmit={formik.handleSubmit}>
                      <div class="form-group mb-3">
                        <TextField
                          id="inputtext"
                          type="text"
                          label={label}
                          variant="outlined"
                          className="input"
                          name="_username"
                          placeholder={label}
                          value={formik.values._username}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={
                            formik.errors._username && formik.touched._username
                          }
                          helperText={
                            formik.touched._username
                              ? formik.errors._username
                              : null
                          }
                        />
                      </div>
                      <div class="form-group mb-3">
                        <TextField
                          id="inputPassword"
                          type="password"
                          label={label2}
                          variant="outlined"
                          className="input"
                          name="_password"
                          placeholder={label2}
                          value={formik.values._password}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          error={
                            formik.errors._password && formik.touched._password
                          }
                          helperText={
                            formik.touched._password
                              ? formik.errors._password
                              : null
                          }
                        />
                      </div>
                      <br />
                      <br />
                      {localStorage.getItem("idioma") === "Español" ? (
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                          class="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm"
                          disabled={loadingSubmit}
                        >
                          Acceder
                        </Button>
                      ) : (
                        <Button
                          variant="contained"
                          color="primary"
                          type="submit"
                          class="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm"
                          disabled={loadingSubmit}
                        >
                          Access
                        </Button>
                      )}
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {error && (
        <Snackbar open={!!error} onClose={() => setError(null)}>
          <SnackbarContent className="error" message={error} />
        </Snackbar>
      )}
    </div>
  );
};

export default Login;
