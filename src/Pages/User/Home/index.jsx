import React from "react";
import { Redirect } from "react-router-dom";
import Navegation from "../../../Components/Navegation";
import Panel from "../../../Components/Panel";
import foto from "./tecnicas.jpg";

function Home() {
  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA") {
        return <Redirect to="/home" />;
      }
    }
  }
  return (
    <div className="Home">
      <Navegation />
      <Panel />
      <br />
      <div class="jumbotron bg-dark">
        <img src={foto} alt="" width="260" height="260" />
        </div>
   </div> 
);
}
export default Home;
