import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { NotificacionesService } from "services/NotificacionesService";
import { ValidatorService } from "services/ValidatorService";
import Panel from "../../../Components/Panel";
import Button2 from "Components/Button";
import Grid from "@material-ui/core/Grid";
import Navegation from "../../../Components/Navegation";
import AlertDialog from "Components/AlertDialog";
import CircularProgress from "@material-ui/core/CircularProgress";
import ConfirmationDialog from "Components/ConfirmationDialog";
import Typography from "@material-ui/core/Typography";
import LoadingRequest from "Components/LoadingRequest";
import { Snackbar, SnackbarContent } from "@material-ui/core";

function Notificacion() {
  const [currentsugerencia, setCurrentsugerencia] = useState([]);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [error, setError] = useState(null);
  const [dataAct, setDataAct] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [closeAlert, setCloseAlert] = useState(false);
  const [closeAlert2, setCloseAlert2] = useState(false);
  const [openAlert2, setOpenAlert2] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [openInconfirm, setOpenInconfirm] = useState(false);
  const [notificaciones, setNotificaciones] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadingAction, setLoadingAction] = useState(false);

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  useEffect(() => {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        setLoading(true);
        NotificacionesService.listadoNotificaciones(proyectoid)
          .then((data) => {
            setNotificaciones(data.data);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
          });
      }
    });
  }, [openAlert, openAlert2]);

  function handleConfirm(sugerencia) {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setOpenConfirm(true);
        setCurrentsugerencia(sugerencia);
      } else {
        window.location.reload();
      }
    });
  }

  function handleInConfirm(sugerencia) {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setOpenInconfirm(true);
        setCurrentsugerencia(sugerencia);
      } else {
        window.location.reload();
      }
    });
  }

  function aceptarSugerencia() {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setLoadingSubmit(true);
        setLoadingAction(true);
        setOpenConfirm(false);
        const respuesta = "OK";
        const idsugerencia = currentsugerencia.id;
        NotificacionesService.confirmarNotificacion(idsugerencia, respuesta)
          .then((data) => {
            setOpenAlert(false);
            setOpenAlert(true);
            setCloseAlert(true);
            setDataAct(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
          })
          .catch((error) => {
            if (localStorage.getItem("idioma") === "Español") {
              setError(
                "Ha ocurrido un error en el servidor. Intentelo de nuevo mas tarde"
              );
            } else {
              setError(
                "An error has occurred on the server. Try it again later"
              );
            }
            console.log(error);
            setLoadingSubmit(false);
            setLoadingAction(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  function rechazarSugerencia() {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setLoadingSubmit(true);
        setLoadingAction(true);
        setOpenInconfirm(false);
        const respuesta = "NO_OK";
        const idsugerencia = currentsugerencia.id;
        NotificacionesService.confirmarNotificacion(idsugerencia, respuesta)
          .then((data) => {
            setOpenAlert2(false);
            setOpenAlert2(true);
            setCloseAlert2(true);
            setDataAct(true);
            setLoadingSubmit(false);
            setLoadingAction(false);
          })
          .catch((error) => {
            if (localStorage.getItem("idioma") === "Español") {
              setError(
                "Ha ocurrido un error en el servidor. Intentelo de nuevo mas tarde"
              );
            } else {
              setError(
                "An error has occurred on the server. Try it again later"
              );
            }
            console.log(error);
            setLoadingSubmit(false);
            setLoadingAction(false);
          });
      } else {
        window.location.reload();
      }
    });
  }

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setCloseAlert(false);
      }}
      description={
        localStorage.getItem("idioma") === "Español"
          ? "Operacion Exitosa: La sugerencia fue aceptada. Tu requerimiento ha pasado a produccion"
          : "Successful operation: The suggestion was accepted. Your request has gone into production"
      }
      actionAccept={() => setCloseAlert(false)}
      title=""
      alertType="success"
    />
  );

  const alert2 = (
    <AlertDialog
      open={openAlert2}
      handleClose={() => {
        setCloseAlert2(false);
      }}
      description={
        localStorage.getItem("idioma") === "Español"
          ? "La sugerencia fue rechazada. Le informaremos a los analistas para que vuelvan a sugerirte otro cambio para tu requerimiento"
          : "The suggestion was rejected. We will inform the analysts to suggest another change for your requirement"
      }
      actionAccept={() => setCloseAlert2(false)}
      title=""
      alertType="success"
    />
  );

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA") {
        return <Redirect to="/home" />;
      }
    }
  }

  return (
    <div className="App">
      <Navegation data={dataAct} />
      <Panel />
      <br />
      <br />
      <Typography variant="h5" gutterBottom>
        {localStorage.getItem("idioma") === "Español" ? (
          <strong>
            En esta seccion podra consultar sus notificaciones pendientes. Las
            mismas muestran los reajustes por parte de los analistas en aquellos
            requerimientos que ha registrado.{" "}
          </strong>
        ) : (
          <strong>
            In this section you can check your pending notifications. The
            notifications show the readjustments by the analysts in those
            requirements that you have registered.{" "}
          </strong>
        )}
      </Typography>
      <LoadingRequest open={loadingAction} />
      <ConfirmationDialog
        open={openConfirm}
        onConfirm={aceptarSugerencia}
        onClose={() => setOpenConfirm(false)}
        message={
          localStorage.getItem("idioma") === "Español"
            ? "¿Esta seguro que desea aceptar esta sugerencia?"
            : "Are you sure you want to accept this suggestion?"
        }
      />
      <ConfirmationDialog
        open={openInconfirm}
        onConfirm={rechazarSugerencia}
        onClose={() => setOpenInconfirm(false)}
        message={
          localStorage.getItem("idioma") === "Español"
            ? "¿Esta seguro que desea rechazar esta sugerencia?"
            : "Are you sure you want to reject this suggestion?"
        }
      />
      <br />
      {loading && (
        <Grid container direction="row" justify="center" alignItems="center">
          <CircularProgress />
        </Grid>
      )}
      <br />
      <br />
      {!loading &&
        notificaciones &&
        notificaciones.map((notificacion) => {
          return (           
            <div class="jumbotron bg-dark text-white">
              <div class="form-group">
                <label for="exampleFormControlSelect1">
                  {localStorage.getItem("idioma") === "Español" ? (
                    <h4>
                      <strong>Requerimiento Original</strong>
                    </h4>
                  ) : (
                    <h4>
                      <strong>Original Requirement</strong>
                    </h4>
                  )}
                </label>
                <br />
                <textarea
                  class="text-white"
                  name="descripcion"
                  rows="5"
                  cols="50"
                  disabled
                >
                  {notificacion.original}
                </textarea>
              </div>
              <label for="exampleFormControlSelect1">
                {localStorage.getItem("idioma") === "Español" ? (
                  <h4>
                    <strong>Sugerencia de Analista</strong>
                  </h4>
                ) : (
                  <h4>
                    <strong>Analyst Suggestion</strong>
                  </h4>
                )}
              </label>
              <br />
              <textarea
                class="text-white"
                name="descripcion"
                rows="5"
                cols="50"
                disabled
              >
                {notificacion.texto}
              </textarea>
              <br />
              <br />
              <Grid container spacing={4} justify="center" alignItems="center">
                <Grid item xs={4}>
                  {localStorage.getItem("idioma") === "Español" ? (
                    <Button2
                      label="Aceptar"
                      variant={"contained"}
                      color={"success"}
                      size="small"
                      handleClick={() => handleConfirm(notificacion)}
                      disabled={loadingSubmit}
                    />
                  ) : (
                    <Button2
                      label="Accept"
                      variant={"contained"}
                      color={"success"}
                      size="small"
                      handleClick={() => handleConfirm(notificacion)}
                      disabled={loadingSubmit}
                    />
                  )}
                </Grid>
                <Grid item xs={4}>
                  {localStorage.getItem("idioma") === "Español" ? (
                    <Button2
                      label="Rechazar"
                      variant={"contained"}
                      color={"danger"}
                      size="small"
                      handleClick={() => handleInConfirm(notificacion)}
                      disabled={loadingSubmit}
                    />
                  ) : (
                    <Button2
                      label="Reject"
                      variant={"contained"}
                      color={"danger"}
                      size="small"
                      handleClick={() => handleInConfirm(notificacion)}
                      disabled={loadingSubmit}
                    />
                  )}
                </Grid>
              </Grid>
            </div>
          );
        })}
      {notificaciones.length === 0 &&
        !loading &&
        (localStorage.getItem("idioma") === "Español" ? (
          <div class="jumbotron bg-dark text-white">
            <h3 className="messages">No tienes notificaciones nuevas</h3>
          </div>
        ) : (
          <div class="jumbotron bg-dark text-white">
            <h3 className="messages">No new notifications</h3>
          </div>
        ))}
      {openAlert && closeAlert && alert}
      {openAlert2 && closeAlert2 && alert2}
      {error && (
        <Snackbar open={!!error} onClose={() => setError(null)}>
          <SnackbarContent className="error" message={error} />
        </Snackbar>
      )}
    </div>
  );
}

export default Notificacion;
