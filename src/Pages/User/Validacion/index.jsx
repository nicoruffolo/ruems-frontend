import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { ValidarService } from "../../../services/ValidarRequerimientosService";
import { ValidatorService } from "services/ValidatorService";
import { useHistory } from "react-router-dom";
import Panel from "../../../Components/Panel";
import CountdownApp from "Components/CustomTimer";
import Navegation from "../../../Components/Navegation";
import Button from "@material-ui/core/Button";
import TableCell from "@material-ui/core/TableCell";
import AlertDialog from "Components/AlertDialog";
import TableRow from "@material-ui/core/TableRow";
import Button2 from "Components/Button";
import Icon from "@material-ui/core/Icon";
import LoadingRequest from "../../../Components/LoadingRequest";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Snackbar, SnackbarContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  root: {
    width: "100%",
    maxWidth: 500,
  },
}));

function Validacion() {
  const classes = useStyles();
  const history = useHistory();
  const [error, setError] = useState(null);
  const [segundos, setSegundos] = useState(null);
  const [loading, setLoading] = useState(false);
  const [carga, setCarga] = useState(false);
  const [loadingdos, setLoadingdos] = useState(false);
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);
  const [openAlertdos, setOpenAlertdos] = useState(false);

  const [validar, setValidar] = useState([]);

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  function recuperarOraciones(idProyecto) {
    ValidarService.ListadoRequerimientos(idProyecto)
      .then((data) => {
        setValidar(data.data);
        setCarga(true);
        setLoading(false);
        setLoadingSubmit(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
        setLoadingSubmit(false);
      });
  }

  function diferenciaDeFechas(unaFecha) {
    const ultima = new Date(unaFecha);
    const esperada = new Date(ultima);
    esperada.setDate(esperada.getDate() + 1);
    var indianTimeZoneVal = new Date().toLocaleString("en-US", {
      timeZone: "America/Argentina/Buenos_Aires",
    });
    var actual = new Date(indianTimeZoneVal);
    actual.setHours(actual.getHours() + 5);
    var dif = esperada.getTime() - actual.getTime();
    var diferencia_segundos = dif / 1000;
    return Number(String(diferencia_segundos).split(".")[0]);
  }

  useEffect(() => {
    setLoadingdos(true);
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        const proyecto = JSON.parse(localStorage.getItem("proyecto"));
        const proyectoid = proyecto.id;
        setLoading(true);
        setLoadingSubmit(true);
        ValidarService.ultimaVotacion(proyectoid)
          .then((response) => {
            if (response.data.length === undefined) {
              const diferencia = diferenciaDeFechas(
                response.data.ultimaVotacion
              );
              if (diferencia > 0) {
                setSegundos(diferencia);
                setLoadingdos(false);
                setLoading(false);
                setLoadingSubmit(false);
              } else {
                recuperarOraciones(proyectoid);
                setLoadingdos(false);
              }
            } else {
              recuperarOraciones(proyectoid);
              setLoadingdos(false);
            }
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
            setLoadingSubmit(false);
          });
      }
    });
  }, []);

  const alert = (
    <AlertDialog
      open={openAlert}
      handleClose={() => {
        setOpenAlert(false);
      }}
      description={
        localStorage.getItem("idioma") === "Español"
          ? "¿Seguro que desea abandonar esta pagina?"
          : "Are you sure you want to leave this page?"
      }
      actionAccept={() => redirigir()}
      title=""
      alertType="error"
    />
  );

  const alertdos = (
    <AlertDialog
      open={openAlertdos}
      handleClose={() => {
        history.push("/inicio");
      }}
      description="Has votado los requerimientos. ¡Gracias por tu colaboracion!"
      actionAccept={() => history.push("/inicio")}
      title=""
      alertType="success"
    />
  );

  function redirigir() {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        history.push("/inicio");
      } else {
        window.location.reload();
      }
    });
  }

  function onCancel() {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setOpenAlert(true);
      } else {
        window.location.reload();
      }
    });
  }

  function onSubmitAction() {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        const data = [];
        let json = {};
        validar.forEach((element) => {
          var radios = document.getElementsByName(`respuesta${element.id}`);
          for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
              json.id_oracion = element.id;
              json.respuesta = radios[i].value;
              data.push(json);
              json = {};
            }
          }
        });
        if (validar.length !== data.length) {
          if (localStorage.getItem("idioma") === "Español") {
            setError("Por Favor responda todos los requerimientos del listado");
          } else {
            setError("Answer all the requirements in the listing");
          }
        } else {
          setLoadingdos(true);
          ValidarService.VotarRequerimiento(data)
            .then((response) => {
              setLoadingdos(false);
              setOpenAlertdos(true);
            })
            .catch((error) => {
              setLoadingdos(false);
            });
        }
      } else {
        window.location.reload();
      }
    });
  }

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA") {
        return <Redirect to="/home" />;
      }
    }
  }

  return (
    <div className="App">
      <Navegation />
      <Panel />
      <br />
      <br />
      <br />
      <LoadingRequest open={loadingdos} />
      {!loadingdos && !carga && !segundos && <h4>Cargando....</h4>}
      {segundos && <CountdownApp valor={segundos} />}
      {!segundos && carga && (
        <form id="form" class="form">
          <div class="container">
            <table class="table table-striped table-dark">
              <thead>
                {localStorage.getItem("idioma") === "Español" ? (
                  <tr>
                    <th scope="col">requerimiento/regla de negocio</th>
                    <th scope="col">Respuesta</th>
                  </tr>
                ) : (
                  <tr>
                    <th scope="col">requirement/business rule</th>
                    <th scope="col">Answer</th>
                  </tr>
                )}
              </thead>
              <tbody>
                {loading && (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      <CircularProgress />
                    </TableCell>
                  </TableRow>
                )}
                {!loading &&
                  validar &&
                  validar.map((valida) => {
                    return (
                      <tr key={valida.id}>
                        <td>{valida.texto}</td>
                        <td>
                          <div class="form-check form-check-inline">
                            <input
                              type="radio"
                              name={`respuesta${valida.id}`}
                              id="inlineRadio1"
                              value="Co"
                            />
                            {localStorage.getItem("idioma") === "Español" ? (
                              <label
                                class="form-check-label"
                                for="inlineRadio1"
                              >
                                Correcto
                              </label>
                            ) : (
                              <label
                                class="form-check-label"
                                for="inlineRadio1"
                              >
                                True
                              </label>
                            )}
                          </div>
                          <div class="form-check form-check-inline">
                            <input
                              type="radio"
                              name={`respuesta${valida.id}`}
                              id="inlineRadio2"
                              value="In"
                            />
                            {localStorage.getItem("idioma") === "Español" ? (
                              <label
                                class="form-check-label"
                                for="inlineRadio2"
                              >
                                Incorrecto
                              </label>
                            ) : (
                              <label
                                class="form-check-label"
                                for="inlineRadio2"
                              >
                                False
                              </label>
                            )}
                          </div>
                          <div class="form-check form-check-inline">
                            <input
                              type="radio"
                              name={`respuesta${valida.id}`}
                              id="inlineRadio3"
                              value="No"
                            />
                            {localStorage.getItem("idioma") === "Español" ? (
                              <label
                                class="form-check-label"
                                for="inlineRadio3"
                              >
                                NO SE
                              </label>
                            ) : (
                              <label
                                class="form-check-label"
                                for="inlineRadio3"
                              >
                                Unanswered
                              </label>
                            )}
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                {validar.length === 0 &&
                  !loading &&
                  (localStorage.getItem("idioma") === "Español" ? (
                    <p className="messages">
                      No existen requerimientos para validar
                    </p>
                  ) : (
                    <p className="messages">
                      There are no requirements to validate
                    </p>
                  ))}
              </tbody>
            </table>
          </div>
          <br />
          {localStorage.getItem("idioma") === "Español" ? (
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              endIcon={<Icon>send</Icon>}
              disabled={loadingSubmit}
              onClick={onSubmitAction}
            >
              {" "}
              Enviar{" "}
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              className={classes.button}
              endIcon={<Icon>send</Icon>}
              disabled={loadingSubmit}
              onClick={onSubmitAction}
            >
              {" "}
              Send{" "}
            </Button>
          )}
          {localStorage.getItem("idioma") === "Español" ? (
            <Button2
              label="Cancelar"
              variant={"contained"}
              color={"warning"}
              size="small"
              handleClick={onCancel}
              disabled={loadingSubmit}
            />
          ) : (
            <Button2
              label="Cancel"
              variant={"contained"}
              color={"warning"}
              size="small"
              handleClick={onCancel}
              disabled={loadingSubmit}
            />
          )}
        </form>
      )}
      {openAlert && alert}
      {error && (
        <Snackbar open={!!error} onClose={() => setError(null)}>
          <SnackbarContent className="error" message={error} />
        </Snackbar>
      )}
      {openAlertdos && alertdos}
    </div>
  );
}
export default Validacion;
