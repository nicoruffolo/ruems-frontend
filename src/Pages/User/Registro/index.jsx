import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { ValidatorService } from "services/ValidatorService";
import Panel from "../../../Components/Panel";
import Navegation from "../../../Components/Navegation";
import "./styles.css";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  root: {
    width: "100%",
    maxWidth: 500,
  },
}));
function Registro() {
  const classes = useStyles();
  const [permiso, setPermiso] = useState(null);

  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA") {
        return <Redirect to="/home" />;
      }
    }
  }
  return (
    <div className="App">
      <Navegation />
      <Panel />
      <br />
      {localStorage.getItem("idioma") === "Español" ? (
        <Typography variant="h4" gutterBottom>
          Ayudanos a seguir creciendo cada dia mas
        </Typography>
      ) : (
        <Typography variant="h4" gutterBottom>
          Help us to keep growing a little more
        </Typography>
      )}
      <br />
      {localStorage.getItem("idioma") === "Español" ? (
        <Typography variant="h6" gutterBottom>
          Ingrese una oracion en el cuadro de texto. La misma podria ser
          ofrecida a otros usuarios para que la evaluen. Pero antes de hacerlo,
          se recomienda leer nuestras reglas.
        </Typography>
      ) : (
        <Typography variant="h6" gutterBottom>
          Enter a sentence in the text box. The sentence could be offered to
          other users for evaluation. But before doing so, we recommend that you
          read our rules.
        </Typography>
      )}
      <textarea
        id="text"
        spellcheck="true"
        rows="3"
        aria-label="minimum-height"
      >
        {" "}
      </textarea>
      <br />
      {localStorage.getItem("idioma") === "Español" ? (
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          endIcon={<Icon>send</Icon>}
        >
          {" "}
          Enviar{" "}
        </Button>
      ) : (
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          endIcon={<Icon>send</Icon>}
        >
          {" "}
          Send{" "}
        </Button>
      )}
    </div>
  );
}

export default Registro;
