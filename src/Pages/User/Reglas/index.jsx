import React, { useState, useEffect } from "react";
import { AyudaService } from "../../../services/AyudaService";
import { ValidatorService } from "services/ValidatorService";
import { Redirect } from "react-router-dom";
import Panel from "../../../Components/Panel";
import Navegation from "../../../Components/Navegation";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const style = {
  backgroundColor: "orange",
};

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const ListarAyudasView = () => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [ayudas, setAyudas] = useState([]);

  
  if (localStorage.getItem("idioma") === null) {
    localStorage.setItem("idioma", "Español");
  }
  
  
  useEffect(() => {
    ValidatorService.permisoUsuario().then((response) => {
      if (response === true) {
        setLoading(true);
        AyudaService.ListarAyudas()
          .then((data) => {
            setAyudas(data);
            setLoading(false);
          })
          .catch((error) => {
            console.log(error);
            setLoading(false);
          });
      }
    });
  }, []);

  if (localStorage.getItem("access") == null) {
    return <Redirect to="/" />;
  } else {
    if (localStorage.getItem("proyecto") == null) {
      return <Redirect to="/proyectos" />;
    } else {
      if (JSON.parse(localStorage.getItem("currentUser")).rol === "ANALISTA") {
        return <Redirect to="/home" />;
      }
    }
  }

  return (
    <div>
      {console.log(ayudas)}
      <Navegation></Navegation>
      <Panel></Panel>
      <br />
      {localStorage.getItem("idioma") === "Español" ? (
        <Typography variant="h4" gutterBottom>
          A continuacion podra consultar nuestras reglas para escribir oraciones
          adecuadas al registrar nuevos requerimientos.
        </Typography>
      ) : (
        <Typography variant="h4" gutterBottom>
          Below you can see our rules for writing satisfactory requirements when
          registering new sentences.
        </Typography>
      )}
      <br></br>
      <div className="jumbotron" style={style}>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              {localStorage.getItem("idioma") === "Español" ? (
                <TableRow>
                  <StyledTableCell>Regla</StyledTableCell>
                  <StyledTableCell align="center">#Regla</StyledTableCell>
                </TableRow>
              ) : (
                <TableRow>
                  <StyledTableCell>Rule</StyledTableCell>
                  <StyledTableCell align="center">#Rule</StyledTableCell>
                </TableRow>
              )}
            </TableHead>
            <TableBody>
              {loading && (
                <TableRow>
                  <TableCell colSpan={7} align="center">
                    <CircularProgress />
                  </TableCell>
                </TableRow>
              )}
              {!loading &&
                ayudas &&
                ayudas.map((ayuda) => {
                  return (
                    <StyledTableRow>
                      <StyledTableCell component="th" scope="row">
                        {ayuda.texto}
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        {ayuda.numero_ayuda}
                      </StyledTableCell>
                    </StyledTableRow>
                  );
                })}
              {ayudas.length === 0 &&
                !loading &&
                (localStorage.getItem("idioma") === "Español" ? (
                  <p className="messages">No existen ayudas para mostrar</p>
                ) : (
                  <p className="messages">No rules registered</p>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
};
export default ListarAyudasView;
