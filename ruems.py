import spacy

nlp = spacy.load("es_core_news_sm")
doc = nlp("Los trasplantes, como los de corazón o pulmón, salvan la vida del paciente y otros, como los trasplantes de córnea, mejoran su calidad de vida.")
print(doc.text)
for token in doc:
    print(token.text, token.pos_, token.dep_)


    